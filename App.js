import React, { Component } from "react";
import {Platform,View,TouchableWithoutFeedback} from 'react-native'
import { createStackNavigator,createMaterialTopTabNavigator,createAppContainer,createSwitchNavigator } from "react-navigation";
import Login from "./src/container/Login_SignUp/Login"
import Location from './src/container/Location'
import More from './src/container/MainScreen/More/More'
import AddPost from './src/container/MainScreen/AddPost/AddPost'
import Saved from './src/container/MainScreen/More/Saved/Saved'
import Categories from './src/container/MainScreen/Categories/Categories'
import CategoryFeeds from './src/container/MainScreen/Categories/CategoryFeeds'
import Profile from './src/container/MainScreen/Profile/Profile'
import NotificationList from './src/container/Notification/NotificationList'
import AntDesign from 'react-native-vector-icons/AntDesign'
import Entypo from 'react-native-vector-icons/Entypo'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import PlusBtn from "./src/AppComps/PlusBtn";
import Home from "./src/container/MainScreen/Home/Home";
import WelcomeSwiper from './src/container/WelcomeSwiper/WelcomeSwiper'
import LocationPicker from './src/container/LocationPicker/LocationPicker'
import UploadMedia from "./src/container/MainScreen/AddPost/UploadMedia/UploadMedia";
import Followers from "./src/container/MainScreen/Profile/Followers";
import Following from "./src/container/MainScreen/Profile/Following";
import MyPosts from "./src/container/MainScreen/Profile/MyPosts";
import EditProfile from "./src/container/MainScreen/Profile/EditProfile";
import NavigationService from "./src/container/Helper/NavigationService";
import Chat from "./src/container/MainScreen/Chat/Chat";
import Activity from "./src/container/MainScreen/More/Activity/Activity"
import Settings from "./src/container/MainScreen/More/Settings/Settings";
import Blocked_users from "./src/container/MainScreen/More/Settings/Local/Blocked_users";
import Change_password from "./src/container/MainScreen/More/Settings/Local/Change_password";
import Contact_us from "./src/container/MainScreen/More/Settings/Local/Contact_us";
import Delete_account from "./src/container/MainScreen/More/Settings/Local/Delete_account";
import Edit_email from "./src/container/MainScreen/More/Settings/Local/Edit_email";
import Feedback from "./src/container/MainScreen/More/Settings/Local/Feedback";
import Hidden_feeds from "./src/container/MainScreen/More/Settings/Local/Hidden_feeds";
import Licences from "./src/container/MainScreen/More/Settings/Local/Licences";
import Mob_num from "./src/container/MainScreen/More/Settings/Local/Mobile_num";
import Report from "./src/container/MainScreen/More/Settings/Local/Report";
import Terms_and_condition from "./src/container/MainScreen/More/Settings/Local/Terms_and_condition";
import User_id from "./src/container/MainScreen/More/Settings/Local/User_id";
import OtherUserProfile from './src/container/MainScreen/Profile/OtherUserProfile'
import Comments from './src/container/MainScreen/Home/Comments/Comments';



let tabIconsColor = '#545454'
class App extends Component {

  state = {
    isActionEnabled:false,
  }

  closeActionBtn(){
    this.setState({isActionEnabled:false,})
  }

  toggleAction = () => {
    this.setState({isActionEnabled:!this.state.isActionEnabled})
  }

  render() {
    return (
      <View style={{flex:1}}>
        <AppContainer ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }} />

        {/* {
          this.state.isActionEnabled &&
          <TouchableWithoutFeedback onPress={()=>this.closeActionBtn()}>
            <View 
            style={styles.backgroundOverlay_actionBtn} />
            </TouchableWithoutFeedback>
        } */}
        <View pointerEvents='none' style={{width:global.deviceWidth,justifyContent:'flex-end',bottom : 35, position:'absolute'}}>
          <PlusBtn navigation={this.props.navigation}  />
        </View>

        <View style={{position:'absolute',bottom:80,right:40,width:30,height:40}}>
          <More navigation={this.props.navigation} isActionEnabled={this.state.isActionEnabled} actionToggler={this.toggleAction.bind(this)} />
        </View>
      </View>
    )
  }
}

const styles = {
  backgroundOverlay_actionBtn:{
    position:'absolute',
    width:global.deviceWidth,
    height:global.deviceHeight,
    zIndex:1,
    backgroundColor:'rgba(150,150,150,0.6)'
  }
};

export default App

const HomeStack = createStackNavigator({
  home: {
    screen: Home,
    navigationOptions: {
      header: null,
    }
  },

  CategoryFeeds: CategoryFeeds,
  NotifictionList: NotificationList,
  Saved: Saved,
  AddPost: {
    screen: AddPost,
    navigationOptions: {
      header: null
    }
  },

  locationPicker: {
    screen: LocationPicker
  },

  notifications: {
    screen: NotificationList
  },

  otherUserProfile: {
    screen: OtherUserProfile
  },

  otherFollowers: {
    screen: Followers,
    navigationOptions: {
      title: 'Followers'
    },
  },


  otherFollowing: {
    screen: Following,
    navigationOptions: {
      title: 'Following'
    },
  },


  othersPosts: {
    screen: MyPosts,
    navigationOptions: {
      title: 'Posts'
    },
  },

  comments:{
    screen: Comments,
    navigationOptions: {
      title: 'Comments'
    },
  },

  categories_search: {
    screen: Categories,
    navigationOptions: {
      title: 'Categories'
    },

  },


});


const CategoriesStack = createStackNavigator({

  categories: {
    screen: Categories,
    navigationOptions: {
      title: 'Categories'
    },

  },

  CategoryFeeds: {
    screen: CategoryFeeds,
  },
  otherUserProfile: {
    screen: OtherUserProfile
  }
});


const MoreStack = createStackNavigator({


  // More: {
  //   screen: More,
  //   navigationOptions: {
  //     title: 'Profile'
  //   },
  // },

  Profile: {
    screen: Profile,
    navigationOptions: {
      title: 'Profile'
    },
  },

  Categories_More: {
    screen: Categories,
    navigationOptions: {
      title: 'Select Categories',
    },
  },

  followers: {
    screen: Followers,
    navigationOptions: {
      title: 'Followers'
    },
  },


  following: {
    screen: Following,
    navigationOptions: {
      title: 'Following'
    },
  },

  myposts: {
    screen: MyPosts,
    navigationOptions: {
      title: 'My Posts'
    },
  },


  editprofile: {
    screen: EditProfile,
    navigationOptions: {
      title: 'Edit Profile'
    },
  },

  saved: {
    screen: Saved,
    navigationOptions: {
      title: 'Saved Feeds'
    },
  },


  activity: {
    screen: Activity,
    navigationOptions: {
      title: ' My Activity'
    },
  },

    CategoryFeeds: {
    screen: CategoryFeeds,
  },


  settings: {
    screen: Settings,
    navigationOptions: {
      title: ' My Activity'
    },
  },
  blocked: {
    screen: Blocked_users,
    navigationOptions: {
      title: 'Blocked Users'
    },
  },
  changepassword: {
    screen: Change_password,
    navigationOptions: {
      title: 'Change Password'
    },
  },
  contactus: {
    screen: Contact_us,
    navigationOptions: {
      title: 'Contact Us'
    },
  },
  deleteaccount: {
    screen: Delete_account,
    navigationOptions: {
      title: 'Delete Account'
    },
  },
  editemail: {
    screen: Edit_email,
    navigationOptions: {
      title: 'Edit Email'
    },
  },
  feedback: {
    screen: Feedback,
    navigationOptions: {
      title: 'Feedback'
    },
  },
  hiddenfeeds: {
    screen: Hidden_feeds,
    navigationOptions: {
      title: 'Hidden Feeds'
    },
  },
  licences: {
    screen: Licences,
    navigationOptions: {
      title: 'Licences'
    },
  },
  mobilenumber: {
    screen: Mob_num,
    navigationOptions: {
      title: 'Mobile Number'
    },
  },
  userid: {
    screen: User_id,
    navigationOptions: {
      title: 'User ID'
    },
  },
  terms: {
    screen: Terms_and_condition,
    navigationOptions: {
      title: 'Terms and Conditions'
    },
  },
  otherUserProfile: {
    screen: OtherUserProfile
  }

});

const ChatStack = createStackNavigator({
  chat: {
    screen: Chat,
    navigationOptions: {
      header: null,
    }
  }
});






const PostStack = createStackNavigator({
  AddPost: {
    screen: AddPost,
    navigationOptions: {
      header: null,
    }
  },
  Categories_addPost: {
    screen: Categories,
    navigationOptions: {
      title: 'Select Categories',
    },
  },

  locationPicker: {
    screen: LocationPicker,
    // navigationOptions: {
    //   header: null
    // }
  },

  uploadMedia: {
    screen: UploadMedia,
  }
});

const Home_Tabs = createMaterialTopTabNavigator(
  {
    
    Feeds: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: () => (
          <AntDesign name='home' color={tabIconsColor} size={24} />
        ),
      },
    },

    

    Categories: {
      screen: CategoriesStack,
      navigationOptions: {
        title: 'Categories',
        tabBarLabel: 'Categories',
        tabBarIcon: () => (
          <Entypo name='grid' color={tabIconsColor} size={24} />
        ),
      },
    },

    Adding: {
      screen: PostStack, // Empty screen
      navigationOptions: () => ({
        tabBarIcon: () => (
          null
        ),
        tabBarLabel: '',
        title: '',
        // tabStyle : {positon :"absolute", zIndex : 1000}        
      }),
    },



    Chat: {
      screen: ChatStack,
      navigationOptions: {
        title: 'Chat',
        tabBarLabel: 'Chat',
        tabBarIcon: () => (
          <Entypo name='user' color={tabIconsColor} size={24} />
        ),
      },
    },

    More: {
      screen: MoreStack,
      navigationOptions: {
        title: 'More',
        tabBarLabel: 'More',

        tabBarIcon: () => (
          null
        ),

        tabBarOnPress:({navigation,defaultHandler}) => {

        }
      },
    },
   

    // Profile:{
    //     screen : ProfileStack
    // }
  },
  {
    lazy:true,
    tabBarPosition: 'bottom',
    optimizationsEnabled: true,
    
    tabBarOptions: {
      inactiveBackgroundColor: '#fff',
      showIcon: true,
      style: {
        backgroundColor: '#fff',
        bottom: Platform.OS == 'ios' ? 30 : 0,
      },
      tabStyle: {
        padding: 0,
        height: 60,

        zIndex: 100
      },
      activeTintColor: global.text_color_normal,
      inactiveTintColor: global.text_color_light,
      indicatorStyle: {
        backgroundColor: global.primaryColor
      },
      labelStyle: {
        fontSize: 10,
        padding: 0,
        width: '100%'
      }
    },
  }
);

const AppContainer = createAppContainer(createSwitchNavigator(
  {
    intro: WelcomeSwiper,
    login: Login,
    App: Home_Tabs,
  },
  {
    initialRouteName: 'intro',
  }
));
