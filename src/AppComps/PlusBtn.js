import React,{Component} from 'react'
import {View,Text,TouchableWithoutFeedback} from 'react-native'
 import AntDesign from 'react-native-vector-icons/AntDesign'
 import '../container/Helper/Global'

export default class PlusBtn extends Component {
    render(){
        return(
            <View>
                <TouchableWithoutFeedback onPress={()=>{this.props.navigation.navigate('addPost')}}>

                <View style={{alignItems:'center',justifyContent:'center'}}>
                    
                <View style={styles.circleView_container}>
                    <View style={styles.circleView}>
                        <AntDesign name='plus' color='#fff' size={25} />
                    </View>
                </View>

                    <View style={{width:70,height:80,position:'absolute',}} /> 
                </View>
                </TouchableWithoutFeedback>
            </View>
        )
    }
}

const styles = {
    container:{
        flex:1,
        alignItems: 'center',
    },
    
    circleView_container: {
        width: 45,
        height: 45,
        alignItems:'center',
        justifyContent:'center',
        borderRadius: 100/2,
        backgroundColor: global.primaryColor,
        alignItems:'center',
        justifyContent:'center',
        elevation:10,
        
        },


    circleView: {
    width: 40,
    height: 40,
    borderRadius: 100/2,
    backgroundColor: global.primaryColor,
    alignItems:'center',
    justifyContent:'center',
    }
}