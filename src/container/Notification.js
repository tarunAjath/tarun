import React, { Component } from 'react'
import {View,Text } from 'react-native'
import {ListItem} from "react-native-elements"


class Notification extends Component {
  render() {
    const list = [
      {
        name: 'Vibhuti ',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
        subtitle: 'Quality Analyst'
      },
      {
        name: 'Rahul Wadhwa',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
        subtitle: 'Tester'
      },
      {
      name:'Pulkit Babbar  ',
     avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
     subtitle: 'iOS Developer'
      },
        {
          name: 'Amy Farha',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
          subtitle: 'Vice President'
        },
        {
          name: 'Chris Jackson',
          avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
          subtitle: 'Vice Chairman'
        },
    ]
  
    return (
      <View>
        <Text style={{fontSize:16, color:'gray', margin:5, fontWeight:'bold'}}>Edit</Text>
            {
    list.map((l, i) => (
      <ListItem
        key={i}
        leftAvatar={{ source: { uri: l.avatar_url } }}
        title={l.name}
        subtitle={l.subtitle}
      />
    ))
  }
      </View>
    )
  }
}

export default Notification
