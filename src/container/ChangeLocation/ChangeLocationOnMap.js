import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  AlertIOS,Image
} from 'react-native';
import MapView, {Marker, AnimatedRegion} from 'react-native-maps';
import UserDataHolder from '../../../src/container/Login_SignUp/UserDataHolder'
import marker from '../../assets/images/mapPin.png'

const screen = Dimensions.get('window');

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;




export default class ChangeLocationOnMap extends Component {

  componentDidMount(){
  }


  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          ref={ref => {this.map = ref;}}
          showsUserLocation={true}
          initialRegion={{
            latitude: UserDataHolder.currentLatitude,
            longitude: UserDataHolder.currentLongitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}>
          
          {/* {this.createMarkers()} */}
        </MapView>
        <View style={styles.markerFixed}>
          <Image style={styles.marker} source={marker} />
        </View>
        <View pointerEvents="none" style={styles.members}>
          {/* {this.createMembers()} */}
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.saveNewLocation()}
            style={[styles.bubble, styles.button]}
          >
            <Text style = {{color : "white"}}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%'
  },
  marker: {
    height: 48,
    width: 48
  },
  bubble: {
    flex: 1,
    backgroundColor: '#4A90F4',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
    marginRight: 20,
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
    marginBottom: 40,
  },
  members: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    width: '100%',
    paddingHorizontal: 10,
  },
});