import React, { Component } from "react";
import { View, Text, StyleSheet, Alert, TouchableOpacity } from "react-native";

export default class App extends Component {
  state = {
    location: null
  };

  findCoordinates = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const location = JSON.stringify(position);

        this.setState({ location });
      },
      // error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.findCoordinates}>
          <Text style={styles.welcome}>Find My Coords? Turn on GPS</Text>
          <Text>Location: {this.state.location}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});


// import React,{Component} from 'react'
// import {View,Text,PermissionsAndroid} from 'react-native'
// import LocationView from "react-native-location-view";
// export default class extends Component {


//     state = {
        
//       };

//     componentDidMount(){
      
//     }


//     render(){
//         return(
//           <View style={{flex: 1}}>
//           <LocationView
//             apiKey={"MY_GOOGLE_API_KEY"}
//             initialLocation={{
//               latitude: 37.78825,
//               longitude: -122.4324,
//             }}
//           />
//         </View>
//         )
//     }
// }

// const styles = {
//     container:{
//         flex:1,
//     },
// }
