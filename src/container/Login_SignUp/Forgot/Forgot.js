import React, { Component } from 'react';
import { View, Text ,Image, ScrollView} from 'react-native';
import { Input,Button, Item } from 'native-base';

export default class Forgot extends Component {
    render() {
        return (
            <ScrollView>
                <Image
                style={styles.icon}

                source={require("../../../img/icon_head.png")}
                />
                <Text style={styles.forgot}>Forgot Password </Text>

                <View style={{ marginTop: 10,marginLeft:60,marginRight:60}}>
                <Item>
                    {/* <FontAwesome size={10} style={styles.input} /> */}
                    <Input
                        // onChangeText={text => this.setState({ username: text })}
                        placeholder="Username/Email Id" />
                </Item>

                </View>

                <Button
                rounded
                style={styles.btn}>
                    <Text style={styles.btnText}>Submit</Text>
                </Button>

            </ScrollView>

        );

    }
}
const styles ={

    forgot: {
        fontSize: 20,
        marginTop: 30,
        color: "#4A90F4",
        justifyContent: 'center',
        alignItems: "center",
        textAlign: "center",
    },
    input: {
        padding: 20,
        alignSelf: 'flex-end',
    },
    btn: {
        alignSelf: "center",
        backgroundColor: "#4A90F4",
        marginTop: 25, 
        height: 40,
      },
      btnText: {
        color: "white",
        paddingRight: 51,
        paddingLeft: 51,
        fontSize: 14,
        fontFamily: "Gill Sans"
      },
      icon: {
        alignSelf: "center",
        marginTop: 45,
          width: 80,
         height: 80,
          flex:1,
          resizeMode: 'contain' 
      },
}


