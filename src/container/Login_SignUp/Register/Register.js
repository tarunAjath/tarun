import React, { Component } from "react";
import { Text, View, ScrollView, Image, TouchableOpacity } from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Input, Button, Item } from "native-base";

export default class Register extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          <Image
            style={styles.icon}
            source={require("../../../img/icon_head.png")}
          />

          <Text style={styles.welcome}>Welcome to Nimmio</Text>
          <View style={{ paddingLeft: 20, paddingRight: 20 }}>
            <Item>
              <FontAwesome name="user" size={18} style={styles.input} />
              <Input
              style={{ paddingBottom: 5 }}
                onChangeText={text => this.setState({ username: text })}
                placeholder="Username/Email Id"
              />
            </Item>
            </View>
            <View style={{ paddingLeft: 20, paddingRight: 20 }}>
            <Item>
              <FontAwesome name="envelope" size={18} style={styles.input} />
              <Input
              style={{ paddingBottom: 5 }}
                onChangeText={text => this.setState({ email: text })}
                placeholder="Email"
              />
            </Item>
            </View>
            <View style={{ paddingLeft: 20, paddingRight: 20 }}>
            <Item>
              <FontAwesome name="key" size={18} style={styles.input} />
              <Input
              style={{ paddingBottom: 5 }}
                onChangeText={text => this.setState({ password: text })}
                secureTextEntry={true}
                placeholder="Password"
              />
            </Item>
            </View>
            <Button style={styles.btn}>
              <Text style={styles.btnText}>Sign Up</Text>
            </Button>
          </View>
        
      </ScrollView>
    );
  }
}

const styles = {
  welcome: {
    fontSize: 24,
    color: "black",
    fontWeight: "500",
    padding: 10,
    textAlign: "center",
    alignItems: "center"
  },

  icon: {
    alignSelf: "center",
    marginTop: 45,
      width: 80,
     height: 80,
      flex:1,
      resizeMode: 'contain' 
  },
  btnText: {
    color: "white",
    paddingRight: 60,
    paddingLeft: 60
  },
  btn: {
    alignSelf: "center",
    backgroundColor: "#4A90F4",
    borderRadius: 50,
    marginTop: 30,
    height: 40
  },
  input: {}
};
