import React, { Component } from 'react'
import {TouchableOpacity } from 'react-native'
import FontAwesome from "react-native-vector-icons/FontAwesome";

class Login_fb extends Component {
  render() {
    return (
      <TouchableOpacity style={styles.circle}>
        <FontAwesome name='facebook' size={24} style={styles.socialLoginIcons}/>
      </TouchableOpacity>
    )
  }
}

const styles={
circle:{
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#fff',
    elevation:10,
    marginRight: 10,
    alignItems: 'center',
    justifyContent:'center',  
},
socialLoginIcons:{
    color:'#3b5998',
},
}

export default Login_fb;
