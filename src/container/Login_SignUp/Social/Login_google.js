import React, { Component } from 'react'
import {TouchableOpacity } from 'react-native'
// import { GoogleSignin, GoogleSigninButton ,  statusCodes  } from 'react-native-google-signin';
import FontAwesome from "react-native-vector-icons/FontAwesome";
export default class Login_google extends Component {

  render() 
  {
    // <GoogleSigninButton
    // style={{ width: 192, height: 48 }}
    // size={GoogleSigninButton.Size.Wide}
    // color={GoogleSigninButton.Color.Dark}
    // onPress={this._signIn}
    // disabled={this.state.isSigninInProgress} />

    return (
         <TouchableOpacity style={styles.circle}>
         <FontAwesome name='google' size={24} style={styles.socialLoginIcons}/>
        </TouchableOpacity>
    );
  }
}
   const styles={
   circle:{
       width: 50,
       height: 50,
       borderRadius: 50,
       backgroundColor: '#fff',
       elevation:10,
       marginLeft: 10,
       alignItems: 'center',
       justifyContent:'center'
   },
   socialLoginIcons:{
             color:'#DD4B39',
   },
  }
  

