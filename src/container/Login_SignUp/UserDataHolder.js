import {AsyncStorage} from 'react-native'
import HelperMethods from './../Helper/Methods'

const UserDataHolder = {
     
        access_token: '',
        currentLatitude :'',
        currentLongitude : '',
        currentAddress: '',
        currentRegion : '',
        userId : '',
        subscribe_category:'',
        profile_image:'',
        follower:'',
        following:'',
        numberOfPosts:'',
        name:'',
        email:'',
        about_me: '',
        isLoggedIn:'',
        gender:''
     }
    

export default UserDataHolder
