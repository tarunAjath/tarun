import React, { Component } from "react";
import {
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Keyboard,
  AsyncStorage
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { Input, Button, Item } from "native-base";
import axios from "axios";
import Login_fb from "./Social/Login_fb";
import Login_google from "./Social/Login_google";
import HelperMethods from '../Helper/Methods'
import LottieView from 'lottie-react-native';
import LottieSpinner from "../../Helpers/HOC/LottieSpinner";
const ContainerWithLottie = LottieSpinner(View);
export default class Login extends Component {
  
  state={
    username:'kumar',
    password:'123456789',
    loggingIn:false,
  }
  componentWillMount(){
    this.checkIfLoggedin()
  }

  checkIfLoggedin = async () => {
    try {
      const value = await AsyncStorage.getItem('isLoggedIn');
      if (value !== null && value == 'true') {
        let authToken = await AsyncStorage.getItem('authToken');
        let follower = await AsyncStorage.getItem('follower');
        let following = await AsyncStorage.getItem('following');
        let numberOfPosts = await AsyncStorage.getItem('numberOfPosts');
        let userName = await AsyncStorage.getItem('userName');
        let about_me = await AsyncStorage.getItem('about_me');
        let userEmail = await AsyncStorage.getItem('userEmail');
        let profile_image = await AsyncStorage.getItem('profile_image');
        let date_of_birth = await AsyncStorage.getItem('date_of_birth');
        let gender = await AsyncStorage.getItem('gender');


        let subscribe_category = await AsyncStorage.getItem('subscribedCat');
          await AsyncStorage.getItem('userId').then((id)=>{
            HelperMethods.navigateHome(this.props.navigation,authToken,id,userName,userEmail,subscribe_category,follower,following,numberOfPosts,about_me,profile_image,date_of_birth,gender)   

              })

      } else {
          // this.setState({showLoginIF:true})
      }
    } catch (error) {
      // Error retrieving data
    }
  };


  login = () => {
      if(this.state.username == '' || this.state.password == ''){
        alert('Please enter username & password')
        return
      }
      let username = this.state.username
      let password = this.state.password
      
      const formData = new FormData();
      formData.append("username",username);
      formData.append("password",password);
        
      Keyboard.dismiss()
      this.setState({loggingIn:true})
      HelperMethods.makeNetworkCall_post("login",formData,(response)=>{
        this.setState({loggingIn:false})
        if(response.flag == 1){             
                this.saveUser(response)
            } else if(response.Status == 0) {
                alert("Wrong email or password")
            }
        })
  }

  saveUser =  (response) => {
    let data = response.data
    let subscribedCat = data.subscribe_category
    let userId = data.id
    let about_me = data.about_me
    let authToken = response.access_token
    let follower = data.follower
    let following = data.following
    let numberOfPosts = data.posts
    let userName = data.name
    let userEmail = data.email
    let profile_image = data.profile_image
    let date_of_birth = data.date_of_birth
    let gender = data.gender

    
    HelperMethods.navigateHome(this.props.navigation,authToken,userId,userName,userEmail,subscribedCat,follower,following,numberOfPosts,about_me,profile_image,date_of_birth,gender)  
    
  };

  render() {
    return (
        <ContainerWithLottie
        isApiCall={this.state.loggingIn}
          style={{
            backgroundColor: "#FFFFFF",
            
            flex: 1,
            borderRadius: 7
          }}
        >
          <ScrollView  showsVerticalScrollIndicator={false}>
            <Image
              style={styles.icon}
              source={require("../../img/icon_nimmio.png")}
            />

            <Text style={styles.welcome}>Welcome to Nimmio</Text>

            <View style={{ paddingLeft: 20, paddingRight: 20, marginTop: 20 }}>
              <Item>
                <FontAwesome name="user" size={18} color={"#ADADAD"} />
                <Input
                  style={{ paddingBottom: 5 }}
                  value={this.state.username}
                  onChangeText={text => this.setState({ username: text })}
                  placeholder="Username/Email Id"
                  placeholderTextColor="#ADADAD"
                />
              </Item>
            </View>
            <View style={{ paddingLeft: 20, paddingRight: 20 }}>
              <Item>
                <FontAwesome name="key" size={18} color={"#ADADAD"} />
                <Input
                  style={{ paddingBottom: 5 }}
                  value={this.state.password}
                  onChangeText={text => this.setState({ password: text })}
                  secureTextEntry={true}
                  placeholder="Password"
                  placeholderTextColor="#ADADAD"
                />
              </Item>
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("forgot")}
              style={styles.forgot}
            >
              <Text
                style={{
                  color: "#4A90F4",
                  fontSize: 12,
                  fontFamily: "GillSans-Light"
                }}
              >
                Forgot Password?
              </Text>
            </TouchableOpacity>

            <Button
              rounded
              style={styles.btn}
              onPress={this.login.bind(this)}
              >
              <Text style={styles.btnText}>Login</Text>
            </Button>

            <View style={{ margin: 20 }}>
              <View style={styles.line} />
              <Text style={styles.orText}>or</Text>
            </View>

            <View style={styles.socialLoginContainer}>
              <Login_fb />

              <Login_google />
            </View>

            <View style={styles.register}>
              <Text
                style={{ alignSelf: "center", paddingRight: 2, fontSize: 11 }}
              >
                Don’t have an account yet?
              </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("register")}
              >
                <Text style={{ color: "#4A90F4", fontSize: 11 }}>
                  Register Now
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>

          <View style={styles.terms}>
          <Text style={styles.termsText}>
            By logging in you agree to Nimmios Terms of Services,
          </Text>
          <Text style={styles.termsText}>
            Private Policies & Content Policies
          </Text>
        </View>


        </ContainerWithLottie>
    );
  }
}


const styles = {
  welcome: {
    fontSize: 20,
    color: "#000",
    marginTop: 10,
    textAlign: "center",
    alignItems: "center",
    fontFamily: "GillSans-SemiBold"
  },

  icon: {
    alignSelf: "center",
    marginTop: 36,
    width: 60,
    height: 60
  },
  forgot: {
    alignSelf: "flex-end",
    marginTop: 10,
    fontWeight: "100",
    marginRight:20
  },
  btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
    fontFamily: "Gill Sans"
  },
  btn: {
    alignSelf: "center",
    backgroundColor: "#4A90F4",
    marginTop: 25, 
    height: 40,
  },

  line: {
    alignSelf: "center",
    position: "absolute",
    borderBottomColor: "rgba(112, 112, 112,0.3 )",
    borderBottomWidth: 1,
    height: "50%",
    width: "100%",
    marginTop:8
  },
  orText: {
    alignSelf: "center",
    paddingHorizontal: 6,
    color: "#000",
    fontSize: 14,
    backgroundColor: "#fff",
    fontFamily: "GillSans-SemiBold",
    marginTop:10
  },
  terms: {
    alignItems: "center",
    marginBottom: 10
  },
  register: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    marginTop:20
  },
  socialLoginContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
    marginTop: 10
  },
  termsText: {
    fontSize: 9,
    alignSelf: "center",
    fontFamily: "Gill Sans",
    color: "#848484"
  },
  overlay: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    opacity: 0.5,
    backgroundColor: 'black'
    
  } 
};


