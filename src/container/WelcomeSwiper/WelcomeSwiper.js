import React, { Component } from 'react';
import {
  Text,
  View, ImageBackground, Image, AsyncStorage, TouchableOpacity,
} from 'react-native';
import Swiper from 'react-native-swiper-animated';
import { Button } from "native-base";

export default class WelcomeSwiper extends Component {

  state = {
    renderScreen:false
  }

  componentWillMount(){
    this.checkIfFirstTime()  
  }

  checkIfFirstTime = async () => {
    try {
      const value = await AsyncStorage.getItem('isFirstLaunch');
      if (value == null) { //first launch
          this.setState({renderScreen:true})
      } else if(value == 'true') {
        this.props.navigation.navigate('login')
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  navigateLogin(){
    AsyncStorage.setItem("isFirstLaunch",'true')
    this.props.navigation.navigate('login')
  }

  render() {
    return (
      <View style={{flex:1}}>
        {
          this.state.renderScreen &&

      <ImageBackground style={styles.backgroundImage}
        source={require("../../assets/images/gradientBackground.png")}>

        <Swiper stack={true} showPagination={false} horizontal={true}
          style={styles.wrapper}
          smoothTransition
          loop
        >
          <View style={styles.slide1}>
            <Image
              style={styles.icon}
              source={require("../../assets/images/infoIcon.png")}
            />
            <Text style={{ paddingTop: 50 }}>   Want to know the best home tutors in Model Town?
                Ask here and we will help you find them. Contrary to popular belief,
               Lorem Ipsum is not simply random text.
                </Text>
          </View>
          <View style={styles.slide2}>
            <Image
              style={styles.icon}
              source={require("../../assets/images/infoIcon.png")}
            />
            <Text style={{ paddingTop: 50 }}>   Want to know the best home tutors in Model Town?
                Ask here and we will help you find them. Contrary to popular belief,
               Lorem Ipsum is not simply random text.
                </Text>
          </View>
          <View style={styles.slide3}>
            <Image
              style={styles.icon}
              source={require("../../assets/images/infoIcon.png")}
            />
            <Text style={{ paddingTop: 50 }}>   Want to know the best home tutors in Model Town?
                Ask here and we will help you find them. Contrary to popular belief,
               Lorem Ipsum is not simply random text.
                </Text>
          </View>
        </Swiper>

        <Button
          transparent
          onPress={() => this.navigateLogin()} style={{ alignSelf: "center", marginBottom: 15 }}>
          <Text style={{ color: 'white' }}> skip</Text>
        </Button>

      </ImageBackground>
        }
      </View>

    );
  }
}


const styles = {
  wrapper: {
    backgroundColor: 'clear',
    marginLeft: 20,
    marginRight: 20,
    paddingTop: 50,
    marginBottom : 20,
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent : "center"
  },
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
    },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,

  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 10,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },

  skipButton: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 40,
  },
  icon: {
    justifyContent: 'center',
    alignItems: 'center'
  }
};

