import React,{Component} from 'react'
import {View,PermissionsAndroid,StyleSheet,Platform} from 'react-native'
import LocationView from "react-native-location-view";
import Geolocation from 'react-native-geolocation-service';
import '../Helper/Global'
import LottieView from 'lottie-react-native';
import DataHolder from '../MainScreen/AddPost/Helpers/DataHolder';
import AnimatedRegion from 'react-native-maps';

import UserDataHolder from '../Login_SignUp/UserDataHolder';


export default class extends Component {

    state = {
      latitude:0,
      longitude:0,
      locationMapped : false,
      };

    componentWillMount(){
      alert("Alert location")
      this.requestLocationPermission()
      this.getCurrentLocation()
    }

    static navigationOptions =
    {
      headerTintColor: global.primaryColor,
      title: "Location"
    };

    getCurrentLocation(){

      Geolocation.getCurrentPosition(
        (position) => {
          let long = position.coords.longitude
          let lat = position.coords.latitude
          this.state.latitude = lat
          this.state.longitude = long
          this.setState({longitude:long,latitude:lat,locationMapped:true})
        },
        (error) => {
            alert(error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

   

    async requestLocationPermission() {
      const chckLocationPermission = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (chckLocationPermission === PERMISSIONS.GRANTED) {
        alert("Granteed")
          this.getCurrentLocation()
          return
      } else {
        alert("Not Granteed")
          try {
              const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                  {
                      'title': 'Location access required',
                      'message': 'To locate your property'
                  }
              )
              if (granted === PERMISSIONS.GRANTED) {
                  this.getCurrentLocation()
              } else {
                  this.requestLocationPermission()
              }
          } catch (err) {
              alert(err)
          }
      }
  };

    moveForward(region,long,lat,address,loadedAddress){
      alert(loadedAddress)
      if(loadedAddress){
        DataHolder.longitude = long
        DataHolder.latitude = lat
        DataHolder.locationAddress = address
        this.props.navigation.pop()
      } else {
        alert('Loading your location..')
      }
    }


    render(){
        return(
          <View style={{flex: 1}}>
          {
            this.state.locationMapped ?
            <LocationView
            actionTextStyle={{fontFamily:global.fontFamily_regular}}
            markerColor={global.primaryColor}
            navigation={this.props.navigation}
            apiKey="AIzaSyDgKtqqnhc7vBvPX2yc633MrGj8oO-0DhU"
            actionText='Select this location'
            onLocationSelect={({region,longitude,latitude,address,loadedAddress}) => this.moveForward(region,longitude,latitude,address,loadedAddress) }
            initialLocation={{
              latitude: this.state.latitude,
              longitude: this.state.longitude,
          
            }}
            region = {
              new AnimatedRegion({
                latitude: this.state.latitude,
                longitude: this.state.longitude,
         
              })
            }
          />
            :
            <View style={{flex:1}}>
              <LottieView
                source={require('../../assets/lottieAnims/location.json')}
                autoPlay
                loop
                />
            </View>
          }
        </View>
        )
    }
}

const styles = {
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
}
