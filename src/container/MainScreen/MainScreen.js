import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import {
  Button,
  Container,
  Footer,
  FooterTab,
  Content,
  Icon
} from "native-base";
import HelperMethods from './../Helper/Methods'

import FontAwesome from "react-native-vector-icons/FontAwesome";
export default class MainScreen extends Component {
  render() {
    return (
      <Container>
        <Content />
        <Footer style={{ backgroundColor: "red" }}>
          <FooterTab>
            <Button active 
              color={"white"}>
              <FontAwesome name="home" size={22} color={"white"} />
              <Text style={{ color: "white" }}>Home</Text>
            </Button>
            <Button>
              <FontAwesome name="list-ul" size={22} color={"white"} />
              <Text style={{ color: "white" }}>Categories</Text>  
            </Button>

            {/* AddPost */}
            <Button onPress={() => this.props.navigation.navigate("AddPost")}>
              <FontAwesome name="plus" size={22} color={"white"} />
            </Button>

            <Button>
              <FontAwesome name="comments" size={22} color={"white"} />
              <Text style={{ color: "white" }}>Chat</Text>
            </Button>
            <Button onPress={() => this.props.navigation.navigate("More")}>
              <FontAwesome name="ellipsis-v" size={22} color={"white"} />
              <Text style={{ color: "white" }}>More</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
     
    );
  }
}
