import React, { Component } from 'react'
import { Text, View, PermissionsAndroid,PERMISSIONS, TouchableHighlight,TouchableOpacity,CameraRoll } from 'react-native'
import Camera from 'react-native-camera'
import ImagePicker from 'react-native-image-picker';
import AntDesign from 'react-native-vector-icons/AntDesign'
import DataHolder from '../../Helpers/DataHolder';
import ImageResizer from 'react-native-image-resizer';
import Permissions from 'react-native-permissions';



class OpenCamera extends Component {


  state = {
    cameraPermission: null,
    photoPermission:null
  };


  componentDidMount() {
    Permissions.getTypes()
    this._checkCameraAndPhotos()
  }

  

  _checkCameraAndPhotos = () => {
    Permissions.checkMultiple(['camera', 'photo']).then(response => {
      //response is an object mapping type to permission
      this.setState({
        cameraPermission: response.camera,
        photoPermission: response.photo,
      })
    })
  }


//   takePicture = () => {
//     const options = {};
//     this.camera.capture({ metadata: options })
//     .then((data) => console.log(data))
//     .catch(err => console.error(err));
//  }

  
  render() {
    return (
        <View style={styles.container}>
        <Camera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={Camera.constants.Aspect.fill}>

          <TouchableOpacity onPress={()=>this.takePicture()}>
          <View style={styles.circleView}>
            <AntDesign name='camera' size={22} color="#fff" />
          </View>
          </TouchableOpacity>
          
        </Camera>
      </View>
    )
  }




  takePicture() {
    const options = {
    
    };
    this.camera.capture({metadata: options})
      .then((data) =>  {

        let { mediaUri} = data
        DataHolder.photos.push({mediaUri})
        this.props.navigation.pop()
      }
     
      )
      .catch(err => alert(err));
  }

  compressImage (uri) {
    ImageResizer.createResizedImage(uri, 300, 300, 'JPEG', 100).then((response) => {
      let {uri} = response
      DataHolder.photos.push({uri})
      this.props.navigation.pop()
      // response.uri is the URI of the new image that can now be displayed, uploaded...
      // response.path is the path of the new image
      // response.name is the name of the new image with the extension
      // response.size is the size of the new image
    }).catch((err) => {
      // alert(err)
      // Oops, something went wrong. Check that the filename is correct and
      // inspect err to get more details.
    });
  }

  
  // async requestPhotosPermission() {
  //   try {
  //       const granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
  //         {
  //           'title': 'Grant Access Storage',
  //           'message': 'Storage access is required for selecting and uploading pictures'
  //         }
  //       )
  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         this.setState({storagePermissionGranted:true})
  //       } else {
  //           this.setState({storagePermissionGranted:false})
  //           this.requestPhotosPermission()
  //       }
  //     } catch (err) {
  //       console.warn(err)
  //     }
  // }
  

}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
  },

  circleView: {
    marginBottom:10,
  width: 50,
  height: 50,
  borderRadius: 100/2,
  alignItems:'center',
  justifyContent:'center',

  backgroundColor: global.primaryColor
  },

  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
}

export default OpenCamera
