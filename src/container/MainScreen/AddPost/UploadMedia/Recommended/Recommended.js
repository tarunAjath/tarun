import React , { Component} from 'react';
import {View, StyleSheet} from 'react-native';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import {SearchBar} from 'react-native-elements';


export default class Search extends Component {
    
    render() {
        
        return (
            <Container>
        <Header 
        style={{backgroundColor:'#fff'}}
        searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Type to Search" />
           
          </Item>
          <Button transparent>
            <Text>Search</Text>
          </Button>
        </Header>
      </Container>
        );

    }
}