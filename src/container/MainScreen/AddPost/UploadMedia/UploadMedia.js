import React, { Component } from "react";
import { PermissionsAndroid, Platform } from 'react-native'
import { Container, Header, Tab, Tabs, } from "native-base";
import Gallery from "./Gallery/Gallery";
import Recommended from "./Recommended/Recommended";
import OpenCamera from "./Camera/OpenCamera";
import '../../../Helper/Global'
import Permissions from 'react-native-permissions'

export default class UploadMedia extends Component {

  componentDidMount() {

    if (Platform.OS == "ios") {
      Permissions.check('photo').then(response => {
        // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        this.setState({ photoPermission: response })
      })
    } else {
      
      this.requestExternalStoragePermission()
      Permissions.request('camera', {
        rationale: {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
        },
      }).then(response => {
        this.setState({ cameraPermission: response })
      })
    }

  }

  // Check the status of multiple permissions
  _checkCameraAndPhotos = () => {
    Permissions.checkMultiple(['camera', 'photo']).then(response => {
      //response is an object mapping type to permission
      this.setState({
        cameraPermission: response.camera,
        photoPermission: response.photo,
      })
    })
  }
  requestExternalStoragePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'My App Storage Permission',
          message: 'My App needs access to your storage ' +
            'so you can save your photos',
        },
      );
      return granted;
    } catch (err) {
      console.error('Failed to request permission ', err);
      return null;
    }
  };

  _alertForPhotosPermission() {
    Alert.alert(
      'Can we access your photos?',
      'We need access so you can set your profile pic',
      [
        {
          text: 'No way',
          onPress: () => console.log('Permission denied'),
          style: 'cancel',
        },
        this.state.photoPermission == 'undetermined'
          ? { text: 'OK', onPress: this._checkCameraAndPhotos }
          : { text: 'Open Settings', onPress: Permissions.openSettings },
      ],
    )
  }



  render() {
    return (
      <Container>
        {/* <Header hasTabs /> */}
        <Tabs tabBarUnderlineStyle={{ backgroundColor: global.primaryColor }} >
          <Tab
            tabBarUnderlineStyle={global.primaryColor}
            textStyle={{ color: global.text_color_light }}
            activeTextStyle={styles.tabs_text}
            tabStyle={styles.tabs}
            activeTabStyle={styles.tabs_active}
            heading="Camera" >
            <OpenCamera navigation={this.props.navigation} />
          </Tab>
          <Tab
            textStyle={{ color: global.text_color_light }}
            activeTextStyle={styles.tabs_text}
            tabStyle={styles.tabs}
            activeTabStyle={styles.tabs_active}
            heading="Gallery">
            <Gallery navigation={this.props.navigation} />
          </Tab>
          <Tab
            textStyle={{ color: global.text_color_light }}
            activeTextStyle={styles.tabs_text}
            tabStyle={styles.tabs}
            activeTabStyle={styles.tabs_active}
            heading="Recommended">
            <Recommended navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

let styles = {
  tabs_text: {
    color: global.text_color_normal,
    fontFamily: global.fontFamily_Regular
  },

  tabs: {
    backgroundColor: '#fff',
    borderBottomWidth: 0.4,
    borderBottomColor: global.text_color_light
  },

  tabs_active: {
    backgroundColor: '#fff',
    borderBottomWidth: 0.4,
    borderBottomColor: global.text_color_light
  },

}

