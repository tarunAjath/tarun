import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback,Image,TouchableOpacity, ScrollView, Switch,FlatList} from 'react-native';
import { Input, Item, Card,Button} from 'native-base'
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import '../../Helper/Global'
import HelperMethods from '../../Helper/Methods';
import UserDataHolder from '../../Login_SignUp/UserDataHolder';
import DataHolder from './Helpers/DataHolder';
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner';
import Geolocation from 'react-native-geolocation-service';
import { jsxClosingElement } from '@babel/types';
import { RNS3 } from 'react-native-aws3';

const LottieView = LottieSpinner(View)

let photoUrls = []
export default class AddPost extends Component {

    state = {
        caption:'',
        selectedCats:[],
        isPosting:false,
        isAnonym:false,
        location:'',
        photos:[],
        tags:'',
        isRemovingTag:false,
    }

    setCaption(text){
        this.setState({caption:text})
    }

    componentWillMount(){
        this.getCurrentLocation()
        // this.props.navigation.navigate("uploadMedia")
    }

    componentDidMount(){
        // this.submitPost()
        this.props.navigation.addListener('willFocus',this.willFocus)
        this.props.navigation.addListener('willBlur',this.willBlur)
    }

    willBlur = () => {
    }

    willFocus = () => {
        this.setState({selectedCats:DataHolder.cat,location:DataHolder.locationAddress,photos:DataHolder.photos})
        // global.navigation.navigate('Categories_addPost',{role:'addPost'})
    }

    uploadImageToS3 = () =>{

        DataHolder.photos.map((item,index) => {
            const file = {
                uri: item.mediaUri,
                name: 'photo.jpg',
                type: 'image/jpeg'
              };

            this.uploadPhoto(file)
        })        
    }
    
        uploadPhoto(file){
            const options = {
                keyPrefix: 'photos/',
                bucket: 'nimmio-s3',
                region: 'ap-south-1 ',
                accessKey: 'AKIA2EDSUISQ362ST6ME',
                secretKey: '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',
                successActionStatus: 201
              };

            RNS3.put(file, options).then(response => {
            
                alert(json.stringify(response.body))
                alert( "inside response", response.stringify(JSON.stringify(response)))
              if (response.status !== 201) {
                  alert("Error Uploading image : ", JSON.stringify(error))
                  photoUrls.push(response.url)
                throw new Error('Failed to upload image to S3', response);
              }
              console.log('*** BODY ***', response.body);
              alert(JSON.stringify(response.body))
            });
        }

    submitPost(){
        let tagsArr = []
        console.log ("Indide upload")

        this.uploadImageToS3()
        let tags_splited = this.state.tags.split(',')
        tags_splited.map((tag) => {
            if(tag == ''){
                return
            }
            tagsArr.push({'title':tag})
        })

        const { caption,location } = this.state
        if(caption == '' || DataHolder.cat.length == 0 || location == ''){
            alert('Please fill valid data')
            return
        }
        this.setState({isPosting:true})
        const formData = new FormData()
        formData.append('user_id',UserDataHolder.userId)
        formData.append('access_token',UserDataHolder.access_token)
        formData.append('post_id','0')
        formData.append('is_del','0')
        formData.append('title',this.state.caption)
        formData.append('description',this.state.caption)
        formData.append('categories',JSON.stringify(DataHolder.cat)) // ['34','12']
        formData.append('tags',JSON.stringify(tagsArr)) // [{'title':'a',is_del}]
        formData.append('post_type',UserDataHolder.userId)
        formData.append('files',photoUrls) //if both [{image,img_path,is_video,is_del,thumbnail_key,thumbnail_file}]
        formData.append('keywords',UserDataHolder.userId)
        formData.append('post_approved',1)
        formData.append('lat',DataHolder.latitude)
        formData.append('LONG',DataHolder.longitude)
        formData.append('country','as')
        formData.append('state','UserDataHolder.userId')
        formData.append('city','UserDataHolder.userId')
        formData.append('sub_city',UserDataHolder.userId)
        formData.append('locality',UserDataHolder.userId)
        formData.append('sub_locality',UserDataHolder.userId)
        formData.append('lang','DataHolder.latitude')
        formData.append('timezone','DataHolder.latitude')
        console.log(formData)
        HelperMethods.makeNetworkCall_post('post_feed',formData,(response) => {
            this.setState({isPosting:false})
            if(response.flag == 1){
                DataHolder.locationAddress = ''
                DataHolder.cat = []
                this.setState({tags:'',caption:'',location:'',selectedCats:[]})
                HelperMethods.snackbar('Post added succesfully','OK',()=>{})
            }
        })
    }

    renderSelectedCats = ({item,index}) => {
        return(
            <View>
                <Text style={{color:global.primaryColor,fontFamily:global.fontFamily_Regular}}>{item.category_name}, </Text>
            </View>
        )
    }

    getCurrentLocation(){
        Geolocation.getCurrentPosition(
          (position) => {
            let long = position.coords.longitude
            let lat = position.coords.latitude
                
              this.setState({longitude:long,latitude:lat,locationMapped:true})
          },
          (error) => {
              // See error code charts below.
            //   alert(error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
    }

    selectLocation(){
        this.props.navigation.navigate('locationPicker')
    }

    setTags(text){
        if(text == ' '){
            return
        }

        if(text[text.length -2] == ',' && text[text.length -1] == ' '){
            return
        } else {
            text = text.split(' ').join(',')
            this.setState({tags:text})
        }
    }

    render() {
        return (
            <LottieView isApiCall={this.state.isPosting} style={{flex:1}}>
                <View style={styles.header}>
                    <View style={[styles.appBarInnerView]}>
                        <Button transparent onPress={()=>this.submitPost()}>
                            <Text style={{color:global.primaryColor,fontSize:20,width:100}}>
                                Add Post
                            </Text>
                        </Button>
                    </View>

                    <View style={[styles.appBarInnerView,{position:'absolute',right:-30}]}>
                        <TouchableOpacity onPress={()=>this.submitPost()}>
                            <MaterialIcon name='check' color={global.primaryColor} size={24} />
                        </TouchableOpacity>
                    </View>

                </View>
            <ScrollView keyboardShouldPersistTaps='always' style={{ backgroundColor: '#F4F4F4' }}>

                <View style={{ margin: 15 }}>
                    <View >
                        <Item style={{ backgroundColor: 'white', height: 80, width: '100%', borderRadius: 5, }}>
                            <Input
                            value={this.state.caption}
                                onChangeText={(text)=>this.setCaption(text)}
                                style={{ alignSelf: 'flex-start', fontSize: 14}}
                                placeholder=" Write a caption"
                                placeholderTextColor='#000' />
                        </Item>
                    </View>

                    <TouchableWithoutFeedback 
                    onPress={()=>this.props.navigation.navigate('Categories_addPost',{role:'addPost'})}>
                        <View style={{ backgroundColor: 'white', height: 80, width: '100%', borderRadius: 5, marginTop: 10, }}>
                            <View style={{alignItems:'flex-start',marginLeft:10}}>
                                <Text style={{ fontSize: 14,  marginTop: 15,color: '#000' }}>{this.state.selectedCats.length > 0 ? 'Reselect categories' : 'Select a Category (upto 3)'} </Text>
                                <View style={{marginTop:5}}>
                                    <FlatList 
                                    extraData={this.state}
                                    horizontal
                                    data={this.state.selectedCats}
                                    renderItem={this.renderSelectedCats}
                                    keyExtractor={(id) => id}
                                    />
                                </View>
                            </View>
                                <AntDesign name="right" style={styles.actionButtonIcon} />
                        </View>
                    </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={()=>this.selectLocation()}>
                            <View style={{backgroundColor: 'white',padding:10,borderRadius: 5, marginTop: 10 }}>
                                <View>
                                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={{fontSize: 14, color: global.text_color_normal}}>{this.state.location.length > 0 ? 'Change location' : 'Select location' }</Text>
                                        <MaterialIcon name="edit-location" size={25} style={styles.actionButtonIcon}/>
                                    </View>
                                    <Text style={{fontSize: 14,color: global.primaryColor}}>{this.state.location}</Text>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>

                    <ScrollView horizontal contentContainerStyle={{ alignItems: 'flex-start', marginTop: 10 }}>
                        <TouchableWithoutFeedback
                            onPress={() => this.props.navigation.navigate("uploadMedia")}>
                            <Card style={{ alignItems: 'center', height: 107, padding: 15, borderRadius: 5 }}>
                                <Text style={{ color: '#4A90F4', fontSize: 30 }}>+</Text>
                                <Text>Upload media</Text>
                            </Card>
                        </TouchableWithoutFeedback>

                        {
                            this.state.photos.map((item) => { //these are jst being rendered 
                                return(
                                    <TouchableWithoutFeedback
                                    onPress={() => this.props.navigation.navigate("uploadMedia")}>
                                        <Card style={{ alignItems: 'center', height: 107,width:115,borderRadius: 5 }}>
                                            <Image source={{uri:item.mediaUri}} resizeMode='cover' style={{width:'100%',height:'100%'}} />
                                        </Card>
                                    </TouchableWithoutFeedback>
                                )
                            })
                        }
                    </ScrollView>

                    <View style={{ backgroundColor: 'white', height: 100, width: '100%',marginTop: 10, }}>
                        <Item style={{ backgroundColor: 'white', height: 80, width: '100%'}}>
                            <Input
                            value={this.state.tags}
                                onChangeText={(text)=>this.setTags(text)}
                                style={{ alignSelf: 'flex-start', fontSize: 14}}
                                placeholder="Add tags here"
                                // onKeyPress={(e) => this.detectBackSpace_tags(e) }
                                placeholderTextColor='#000' />
                        </Item>
                    </View>
                    <Card style={{ marginTop:10,borderRadius: 5 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', height: 80, }}>
                            <Switch
                                style={{ justifyContent: 'center' }}
                                onValueChange={(value) =>  this.setState({isAnonym:value}) }
                                value={this.state.isAnonym} />
                            <Text style={{ fontSize: 14 }}>   Post Anonymously</Text>
                        </View>
                    </Card>
                </View>
            </ScrollView>
            </LottieView>
        );
    }
}
const styles = {
    title: {
        fontSize: 16,
        color: '#4A90F4',
        marginTop: 20,
        alignSelf: 'center',
    },
    actionButtonIcon: {
        alignSelf: 'flex-end',
        color: global.text_color_normal,
        marginRight: 10,
    },

    appBarInnerView:{
        width:70,
    },

    header:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        padding:25,
        backgroundColor:'#F4F4F4'
    }
}

