import React, { Component } from 'react'
import { Text, View, FlatList, KeyboardAvoidingView, ScrollView } from 'react-native'
import CommentsCell from './CommentsCell'
import { Input, Item, CheckBox } from 'native-base';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import HelperMethods from '../../../Helper/Methods';
import UserDataHolder from '../../../Login_SignUp/UserDataHolder';
import LottieSpinner from '../../../../Helpers/HOC/LottieSpinner';


const LottieLoader = LottieSpinner(View)

export class Comments extends Component {
    state = {
        comments: [],
        noComments: false,
        isLoadingComments : false
    }
    

    componentWillMount() {
        this.getCommentsData()
    }


    getCommentsData = () => {

        let { cat_name } = this.props.navigation.state.params
        const formData = new FormData();
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", '0d782b3f9c3574bc4abc844715f48f8c');
        formData.append("post_id", cat_name.id);
        formData.append("flag_view", '0');
        //formData.append("offset", '0');
        //formData.append("lang", '');
        formData.append("timezone", '0');

        HelperMethods.makeNetworkCall_post("manage_comment", formData, (response) => {
            this.setState({ isLoadingComments: false })
            if (response.flag == 1) {
                this.setState({ comments: response.data, noComments: false })
            } else {
                this.setState({ noComments: true })
            }
        })
    }



    renderComments = ({ item, index }) => {

        return (
            <CommentsCell
                comment={item.comment}
                comment_date={item.comment_date}
                user_name={item.user_name}
                profile_image={item.profile_image}
                comment_image={item.comment_image}
                total_like={item.total_like}
            />
        )
    }


    render() {
        return (
            <LottieLoader isApiCall={this.state.isLoadingComments} style={{ flex: 1 }}>
<KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
            {/* <ScrollView keyboardShouldPersistTaps="always" contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', flexDirection: 'column' }}> */}

                <View>
                    <FlatList
                        style={{marginBottom : 20 ,}}
                        data={this.state.comments}
                        renderItem={this.renderComments}
                    />
                </View>
                <View style={{
                    marginBottom: 5,
                    bottom: 30
                }}>
                    <View style={styles.bottomView}>
                        <Item style={styles.inputBox}>
                            <Input
                                placeholder="Type message here"
                                style={{ fontSize: 14 }}
                            />
                            <FontAwesome name='paperclip' size={26} style={{ marginRight: 10 }} />
                        </Item>

                        <View style={styles.postBtn}>
                            <FontAwesome name='send-o' size={24} color= {global.primaryColor} />
                        </View>
                    </View>

                    <View style={styles.checkboxStyle}>
                        <CheckBox
                            //checked={true}
                            color='#4A90F4'
                            style={{ borderRadius: 5 }}
                        />
                        <Text style={styles.postAnonymous}>Post as anonymous</Text>
                    </View>
                </View>
       {/* /     </ScrollView> */}
       </KeyboardAvoidingView>

            </LottieLoader>


        )
    }
}

const styles = {
    bottomView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginRight: 15
    },
    postBtn: {
        //  backgroundColor: global.primaryColor,
        borderRadius: 5,
        height: 50,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputBox: {
        backgroundColor: '#d9d9d9',
        marginLeft: 10,
        marginRight: 15,
        borderRadius: 5,
        height: 50,
        width: '80%'
    },
    postAnonymous: {
        marginLeft: 15,
        fontSize: 12,
        alignSelf: 'center'
    },
    checkboxStyle: {
        flexDirection: 'row',
        marginBottom: 5,
        marginTop : 5
    },
}


export default Comments
