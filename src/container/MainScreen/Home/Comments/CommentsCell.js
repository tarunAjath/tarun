import React from 'react'
import { Text, View, Image } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'


const CommentsCell = ({ id, post_id, main_user_id, comment, comment_type, is_edit, comment_date, user_name, profile_image, chat_JID, blocked_by_me, blocked_by_receiver, user_mute, comment_hide, comment_image, thumbnail_image, image_type, comment_img_id, reply_count, reply_data, is_flag, total_like, is_like }) => (

    <View style={styles.container}>

        <View style={{ flexDirection: "row", flex: 1, alignItems: 'center' }}>

            <Image source={{ uri: profile_image }} style={styles.photo} />
            <View>
                <View style={styles.commentBox}>
                    <Text style={styles.username}>{user_name}</Text>
                    <Text style={styles.comment}>{comment}</Text>
                    {   (this.comment_image == null || this.comment_image == "") ?
                       <View>
                       <Text style={{ marginTop: 5, fontSize: 10 }}>{comment_date}</Text>
                       <View style={styles.likePosition}>
                           <View style={styles.like}>
                               <FontAwesome name='heart' size={10} color='red' />
                               <Text style={{ fontSize: 10, marginLeft: 2 }}>0</Text>
                           </View>
                       </View>
                         </View>:
                        <View>
                            <Image source={{ uri: comment_image }} style={styles.commentPhoto} />
                            <Text style={{ marginTop: 5, fontSize: 10 }}>{comment_date}</Text>
                            <View style={styles.likePosition}>
                                <View style={styles.like}>
                                    <FontAwesome name='heart' size={10} color='red' />
                                    <Text style={{ fontSize: 10, marginLeft: 2 }}>0</Text>
                                </View>
                            </View>
                        </View> 
                     
                    }

                </View>
                <View style={styles.like_reply_View}>
                    <Text style={styles.like_reply}>Like</Text>
                    <Text style={styles.like_reply}>Reply</Text>
                </View>
            </View>
        </View>
    </View>
);

const styles = {
    container: {
        //height:40,
        width: global.deviceWidth,
        marginLeft: 10,
        marginTop: 10,
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 25,
    },
    commentBox: {
        backgroundColor: '#d9d9d9',
        marginLeft: 10,
        padding: 10,
        borderRadius: 5,
        marginRight: 70
    },
    username: {
        fontWeight: "bold",
        color: '#000',
        fontSize: 14
    },
    comment: {
        color: '#000',
        marginTop: 5,
        fontSize: 12
    },
    commentPhoto: {
        height: 100,
        width: 100,
        borderRadius: 5,
        marginTop: 5
    },
    likePosition: {
        position: 'absolute',
        marginTop: 170,
        marginLeft: 95
    },
    like: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 10,
        height: 15,
        width: 25,
        alignItems: 'center',
        justifyContent: 'center'
    },
    like_reply_View: {
        flexDirection: 'row',
        marginLeft: 15,
        padding: 5
    },
    like_reply: {
        marginRight: 5,
        fontSize: 10
    },
}

export default CommentsCell
