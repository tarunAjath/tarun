import React,{Component} from 'react'
import {View,Dimensions,Platform,Image,TouchableOpacity} from 'react-native'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import {Item,Input} from 'native-base'
import '../../../Helper/Global'



export default class AppBar extends Component {

    state = {
        searchInput:'',
    }


    navigateNotifications = () => {
        this.props.navigation.navigate('notifications')
    }

    navigateLocationSelector = () => {
        this.props.navigation.navigate('locationPicker')
    }

    search = () => {
        alert('search')
    }

        render(){

        return(
            <View style={styles.container}>
                    <Image
                        style={{ height: 31, width: 31,left:10 }}
                        source={require("../../../img/icon_nimmio.png")}
                    />

                    <View style={{flex:0.9}}>
                        <Item 
                        style={{borderBottomColor:global.text_color_normal,bottom:7}}
                        fixedLabel>
                            <Input 
                            onSubmitEditing={this.search}
                            style={{paddingBottom:0}}
                            placeholder='Search'
                            placeholderTextColor={global.text_color_light}
                            value={this.state.searchInput} 
                            onChangeText={(text)=>this.setState({searchInput:text})} />
                        </Item>
                    </View>

                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.navigateLocationSelector()} style={styles.icons}>
                            <SimpleLineIcons color={global.text_color_normal} name='location-pin' size={20} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>this.navigateNotifications()} style={styles.icons}>
                            <SimpleLineIcons color={global.text_color_normal} name='bell' size={20} />
                        </TouchableOpacity>
                    </View>
                </View>
        )
    }
}

const styles = {
    container:{
        width: Dimensions.get('window').width, 
        flexDirection: 'row', 
        marginTop: Platform.OS == 'ios' ? 30 : 0, 
        alignItems:'center',
        justifyContent:'space-between',
        height: 50 ,

    },
    icons:{
        marginLeft:5,
        marginRight:5
    }
}