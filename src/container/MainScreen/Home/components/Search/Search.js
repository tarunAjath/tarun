import React,{Component} from 'react'
import {View,Text,FlatList,PermissionsAndroid,ActivityIndicator} from 'react-native'
import HelperMethods from '../../../../Helper/Methods';
import FeedsListItem from '../FeedsListItem';
import UserDataHolder from '../../../../Login_SignUp/UserDataHolder';
import FilterDataHolder from '../../Helpers/FilterDataHolder';
import Geolocation from 'react-native-geolocation-service';

let searchQuery

export default class Search extends Component {

    state = {
        results:[],
        longitude:0,
        latitude:0,
        locationMapped:false,
        isSearching:false,
        noResults:false,
    }

    componentWillMount(){
        this.requestLocationPermission()
    }

    componentWillReceiveProps(nextProps){
        let {query,getResults} = nextProps
        if(getResults){
            searchQuery = query
            this.fetchResults()
        }
    }

    getCurrentLocation(){
        Geolocation.getCurrentPosition(
          (position) => {
            let long = position.coords.longitude
            let lat = position.coords.latitude
            
              this.setState({longitude:long,latitude:lat,locationMapped:true})
          },
          (error) => {
              // See error code charts below.
              alert(error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
    }
  
     
  
      async requestLocationPermission() {
        const chckLocationPermission = PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (chckLocationPermission === PermissionsAndroid.RESULTS.GRANTED) {
            this.getCurrentLocation()
            return
        } else {
            try {
                const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        'title': 'Location access required',
                        'message': 'To locate your property'
                    }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.getCurrentLocation()
                } else {
                    this.requestLocationPermission()
                }
            } catch (err) {
                alert(err)
            }
        }
    };


    fetchResults(){
        let lat = this.state.latitude
        let long = this.state.longitude

        let locationObj = {"lat" :lat,"long" :long,"place_id":"place_id","country" :"country1","state" : "state1","city" : "city1","locality" : "locality1","sub_locality" : "sub_locality1"}
        
        let filter_data = {
            "categories" :JSON.stringify(FilterDataHolder.cats),
            "resolved" :FilterDataHolder.resolvedQuery,
            "unresolved" :FilterDataHolder.unresolvedQuery,
            "sort" : FilterDataHolder.sort,
            "range" : FilterDataHolder.timeRange,
            "lat":lat,
            "long":long,
        }
        
        const formData = new FormData()
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append('search_keyword',searchQuery)
        formData.append('location',JSON.stringify(locationObj))
        formData.append('flag_view',0)
        formData.append('lang','Eng')
        formData.append('filter_data',JSON.stringify(filter_data))
        formData.append('timezone','a')

        if(!this.state.locationMapped){
            alert('Please wait getting your location')
            return
        }

        this.setState({isSearching:true})
        HelperMethods.makeNetworkCall_post('posts_search',formData,(response)=>{
            this.setState({isSearching:false})
            if(response.search_data.length == 0){
                this.setState({noResults:true})
                return
            } 
        })
    }

    renderResults = ({item,index}) => {
        return(
            <FeedsListItem
            key={item.id}
            id={item.id}
            user_id = {item.user_id}
            menuActive={this.state.clickedFeedId == item.id}
            showFeedsUIHandler={(id) => this.showAlertBox(id)}
            title={HelperMethods.Capitalize(item.user_name)}
            subtitle={HelperMethods.Capitalize(item.created_date)}
            description={item.description}
            image_url={item.user_image}
            categories={item.categories_array}
            post_image_array={item.images}
            tags_array={item.tags_array}
            currentNavigation = {this.props.navigation} 
            />
        )
    }
    render(){
        return(
            <View style={styles.container}> 
            {this.state.noResults && 
                <Text style={{fontWeight:'bold',fontSize:18,alignSelf:'center'}}>No results found </Text>
            }

            { this.state.isSearching ? 

             <ActivityIndicator  size='large' color={global.primaryColor} />
             :
                <FlatList 
                data={this.state.results}
                renderItem={this.renderResults}
                />
            }
            </View>
        )
    }
}

const styles = {
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent:'center'
    },
}