import React,{Component} from 'react'
import {View,Text,TouchableWithoutFeedback,ScrollView,CheckBox,TouchableOpacity,LayoutAnimation,UIManager,Platform} from 'react-native'
import {Card} from 'native-base'
import FilterDataHolder from '../../Helpers/FilterDataHolder';
import Entypo from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign'

let sortOptions = [
    {'name':'Relevant'},
    {'name':'Recent'},
    {'name':'Popular'},
    {'name':'Nearest'},

]

let timeRangeOptions = [
    {'name':'Any time'},
    {'name':'Past 24 hours'},
    {'name':'Past week'},
    {'name':'Past month'},
    {'name':'Past year'},

]

removeCat = (id,Class) => {
    
    FilterDataHolder.cats.splice(FilterDataHolder.cats.findIndex(v => v.category_id = id),1)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    Class.setState({})
}

const catItem = (item,Class) => (
    <TouchableWithoutFeedback onPress={()=>removeCat(item.category_id,Class)}>

    <View style={styles.catContainer}>
        <Text style={{color:global.primaryColor}}>{item.category_name} </Text>
        <AntDesign name='close' size={18} color={global.primaryColor} />
    </View>
    </TouchableWithoutFeedback>
)

let chevronColor = '#000'
let chevronSize = 19

export default class Filter extends Component {

    state = {
        showSort:false,
        isResolvedQuery:false,
        isunResolvedQuery:false,
        showTimeRange:false,

        selectedSort:'None',
        selectedTimeRange:'None',
    }

    componentWillMount(){
        if(Platform.OS == 'android')
        UIManager.setLayoutAnimationEnabledExperimental(true)
        
        this.setState({selectedSort:FilterDataHolder.sort == '' ? this.state.selectedSort : FilterDataHolder.sort,selectedTimeRange: FilterDataHolder.timeRange == '' ? this.state.selectedTimeRange : FilterDataHolder.timeRange})
        this.setState({isResolvedQuery:FilterDataHolder.resolvedQuery,isunResolvedQuery:FilterDataHolder.unresolvedQuery})
    }

   
    navigateCat(){
        this.props.navigation.navigate('categories_search',{'role':'search'})
    }

    setSort(sort){
        FilterDataHolder.sort = sort
        this.setState({selectedSort:sort})
    }

    toggleSort(){
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
        this.setState({showSort:!this.state.showSort})
    }

    toggleTimeRange(){
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
        this.setState({showTimeRange:!this.state.showTimeRange,})
    }

    setTimeRange(time){
        FilterDataHolder.timeRange = time
        this.setState({selectedTimeRange:time})
    }

    setUnresolvedQuery(val){
        this.setState({ isunResolvedQuery: val },()=>{
            FilterDataHolder.unresolvedQuery = val
        })
    }

    setResolvedQuery(val){
        this.setState({ isResolvedQuery: val },()=>{
            FilterDataHolder.resolvedQuery =  val
        })
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.refreshCats){
            this.setState({})
        }
    }

    render(){
        return(
        <ScrollView keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={'always'}>

            <Card style={styles.filterCard}>
                    <View style={{marginBottom:10}}>
                        <TouchableWithoutFeedback onPress={()=>this.navigateCat()}>
                            <View style={styles.filterTap}>
                                <Text style={styles.text}>Add categories</Text>
                                <Entypo name={'chevron-small-right'} size={chevronSize} color={chevronColor} />
                            </View>
                        </TouchableWithoutFeedback>

                        <View style={{flexDirection:'row',flex:1,flexWrap:'wrap'}}>
                        {FilterDataHolder.cats.map((item) => {
                            return(
                                catItem(item,this)
                            )
                        })}

                        </View>

                    </View>

                    <View style={styles.filterItemContainer}>
                        <TouchableWithoutFeedback onPress={()=>this.toggleSort()}>
                            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                <Text style={styles.text}>Sort - <Text style={{fontWeight:'bold'}}>{this.state.selectedSort} </Text>  </Text>
                                <Entypo name={this.state.showSort ? 'chevron-small-up' : 'chevron-small-down'} size={chevronSize} color={chevronColor} />
                            </View>
                        </TouchableWithoutFeedback>
                        {this.state.showSort && 
                        <View>
                            {sortOptions.map((item) => {
                                return(
                                    <TouchableOpacity onPress={() => this.setSort(item.name)}>
                                        <View style={[{marginTop:10},this.state.selectedSort == item.name ? styles.sortItemSelected : styles.sortItemDefualt ]}>
                                            <Text style={[this.state.selectedSort == item.name ? styles.sortStyleTextSelected : styles.sortStyleTextDefault ]}>{item.name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                        }
                    </View>

                    <View style={styles.filterItemContainer}>
                        <View>
                        <TouchableWithoutFeedback onPress={()=>this.setState({ isResolvedQuery: !this.state.isResolvedQuery })}>
                            <View style={styles.checksRow}>
                                <Text style={styles.sortStyleText}>Resolved Queries</Text>
                                <CheckBox
                                // onChange={(val) => alert(val)}
                                value={this.state.isResolvedQuery}
                                onValueChange={(val) => this.setResolvedQuery(val)}
                                />
                            </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback onPress={()=>this.setState({ isunResolvedQuery: !this.state.isunResolvedQuery })}>
                                <View style={styles.checksRow}>
                                    <Text style={styles.sortStyleText}>Unresolved Queries</Text>
                                    <CheckBox
                                    value={this.state.isunResolvedQuery}
                                    onValueChange={(val) => this.setUnresolvedQuery(val)}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>

                    <View style={styles.filterItemContainer}>
                        <TouchableWithoutFeedback onPress={()=>this.toggleTimeRange()}>
                            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                                <Text style={styles.text}>Time Range - <Text style={{fontWeight:'bold'}}>{this.state.selectedTimeRange}</Text> </Text>
                                <Entypo name={this.state.showTimeRange ? 'chevron-small-up' : 'chevron-small-down'} size={chevronSize} color={chevronColor} />
                            </View>
                        </TouchableWithoutFeedback>
                        {this.state.showTimeRange && 
                        <View>
                            {timeRangeOptions.map((item) => {
                                return(
                                    <TouchableOpacity onPress={() => this.setTimeRange(item.name)}>
                                        <View style={[{padding:10,marginTop:10},this.state.selectedTimeRange == item.name ? styles.sortItemSelected : styles.sortItemDefualt  ]}>
                                            <Text style={[this.state.selectedTimeRange == item.name ? styles.sortStyleTextSelected : styles.sortStyleTextDefault ]}>{item.name}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })}
                        </View>
                        }
                    </View>


            </Card>
</ScrollView>

        )
    }
}

const styles = {

    sortItemDefualt:{
        padding:10
    },

    sortItemSelected:{
        padding:10,
        backgroundColor:global.primaryColor,
        borderRadius:10,
    },

    sortStyleTextDefault:{
        color:global.text_color_normal
    },

    sortStyleTextSelected:{
        color:'#fff'
    },

   

    container:{
        flex:1,
        alignItems: 'center',
    },

    filterCard:{
        borderBottomLeftRadius:5,
        borderBottomRightRadius:5,
        padding:10,
    },

    filterItemContainer:{
        padding:10,
    },

    filterTap:{
        flexDirection:'row',
        padding:10,
        alignItems:'center',
        backgroundColor:'#eee',
        justifyContent:'space-between'
    },

    checksRow:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginBottom:10,
    },

    catContainer:{
        borderRadius:10,
        borderColor:global.primaryColor,
        borderWidth:1,
        padding:7,
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection:'row',
        paddingTop:3,
        paddingBottom:3,
        marginRight:10,
        marginTop:10,
    }

}