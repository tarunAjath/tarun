import React,{Component} from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import { Container, Header, Content, Button, Icon, Text } from 'native-base';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import HelperMethods from "../../../Helper/Methods";
import LinearGradient from 'react-native-linear-gradient';
import Entypo from "react-native-vector-icons/Entypo";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import UserDataHolder from "../../../Login_SignUp/UserDataHolder";
import FontAwesome from 'react-native-vector-icons/dist/FontAwesome'
import AntDesign from 'react-native-vector-icons/AntDesign'


const placeHolderImage = require('../../../../assets/images/userPlaceholder.png')
const bannerPlaceHolderImage = require('../../../../assets/images/bannerPlaceholder.png')

const goToCategoryDetails = (item, currentNavigation) => {
    let params = { 'title': item.category_name, 'id': item.id, }
    if (currentNavigation.state.routeName == 'CategoryFeeds') {
        currentNavigation.push('CategoryFeeds', { 'cat_name': params })
        return
    }
    HelperMethods.navigateToScreen(currentNavigation, params, 'CategoryFeeds')
}

let totalLikeCount 

const goToOtherUserProfiles = (id, currentNavigation) => {
    
    HelperMethods.navigateToScreen(currentNavigation, { 'id': id }, 'otherUserProfile')
}




const saveThePost = (post_id, saveType, updateBookmarkIcons) => {
    updateBookmarkIcons(post_id, saveType)
    const formData = new FormData();
    formData.append("post_id", post_id);
    formData.append("user_id", UserDataHolder.userId);
    formData.append("access_token", UserDataHolder.access_token);
    formData.append("flag_view", 1);
    formData.append("save_type", saveType);
    formData.append("timezone", 'IST');

    HelperMethods.makeNetworkCall_post("post_saved", formData, (response) => {

        if (response.flag == 1) {
        } else if (response.Status == 0) {
        }
    })
}






const setRandomColors = () => {
    const rand = Math.floor((Math.random() * 3) + 1);
    switch (rand) {
        case 1:
            return ['#EE486C', '#FF8B8B']

        case 2:
            return ['#3B7FF1', '#41DFF1']

        case 3:
            return ['#AB72D8', '#7757EB']

        default:
            return ['#EE486C', '#FF8B8B']

    }
}

const setUserImage = (userImageURL) => {

    if (userImageURL) {
        return <Image source={{ uri: userImageURL }} style={styles.photo} />
    } else {
        return <Image source={placeHolderImage} style={styles.photo} />

    }
}

const setBannerImage = (bannerImageURL) => {
    if (bannerImageURL) {
        return <Image source={{ uri: bannerImageURL }} style={styles.postImage} />
    } else {
        return <Image source={bannerPlaceHolderImage} style={styles.postImage} />

    }
}



const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#FFF',
        elevation: 2,
        flex: 1
    },
    topContainer: {
        flexDirection: 'row',
        padding: 16,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    bottomContainer: {

    },
    title: {
        fontSize: 14,
        color: '#000',
    },

    timeStamp: {
        fontSize: 11,
        color: '#717171',
        marginLeft: 15
    },

    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
    },
    container_image: {
        flex: 1,
        justifyContent: 'center',
        resizeMode: 'contain',
    },
    description: {
        fontSize: 15,
        color: '#000',
        padding: 16,
    },

    bannerText: {
        fontSize: 35,
        color: '#000',
        padding: 16,
        fontWeight: "bold",
        alignSelf: "center",
        color: "#fff"
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
    },
    postImage: {
        height: 200,
        marginTop: 12,
        marginLeft: 0,
    },

    postView: {
        height: 200,
        marginTop: 12,
        marginLeft: 0,
        justifyContent: 'center'
    },

    tagButtons: {
        borderColor: "#4A90F4",
        borderRadius: 20,
        marginLeft: 10,
        height: 30,
        alignSelf: 'flex-start'
    },
});


//export default FeedsListItem;
//


export default class  FeedsListItem extends Component {

    state = {
        likeCounts:0,
        likesChanged:false
    }

    componentWillReceiveProps(nextProps,nextState){
        
        if(this.state.likeCounts == 0 && !this.state.likesChanged){
            this.setState({likeCounts:nextProps.total_like})
        }

    }

    goToCommentsScreen = (id,currentNavigtion) =>{
        HelperMethods.navigateToScreen(currentNavigtion, { 'id': id }, 'comments')
    }

 likeThPost = (post_id, likeType, updateLikeIcons) => {
        updateLikeIcons(post_id, likeType)
        const formData = new FormData();
        formData.append("post_id", post_id);
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append("flag_view", 1);
        formData.append("like_type", likeType);
        formData.append("timezone", 'IST');
    
        HelperMethods.makeNetworkCall_post("post_like", formData, (response) => {
            let likesCount  =  parseInt(this.state.likeCounts)
            if (response.flag == 1) {
                if(likeType == 0){
                    this.setState({likeCounts:likesCount+=1,likesChanged:true})
                } else {
                    this.setState({likeCounts:likesCount-=1})
                }
                    
            } else if (response.Status == 0) {
    
            }
        })
    }

render(){
    return(
        <View style={styles.container}>

        <View style={[styles.topContainer, { backgroundColor: this.props.menuActive ? 'rgba(0,0,0,0.5)' : null }]}>
            <View>
                {setUserImage(this.props.image_url)}
            </View>
            <View style={{ marginRight: 20, flex: 1 }}>
                <Button transparent style={{ alignItems: "flex-start" }}
                    disabled={this.props.user_id == '0'}
                    onPress={() => goToOtherUserProfiles(this.props.user_id, this.props.currentNavigation)}
                >
                    <Text style={{ color: global.primaryColor }}>{this.props.title}</Text>
                </Button>

                <Text style={styles.timeStamp}>
                    {this.props.subtitle}
                </Text>
            </View>
            <TouchableOpacity onPress={() => this.props.showFeedsUIHandler(this.props.id)}>
                <Ionicons name="md-more" size={20} color={'black'} />
            </TouchableOpacity>
        </View>
        <View style={styles.bottomContainer}>

            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                {
                    this.props.categories.map((item, index) => {

                        return (
                            <TouchableWithoutFeedback onPress={() => goToCategoryDetails(item, this.props.currentNavigation)}>
                                <View
                                    key={item.category_name}
                                    style={{
                                        borderWidth: 1, borderRadius: 20, borderColor: '#4A90F4', alignItems: 'center', justifyContent: 'center',
                                        margin: 5

                                    }}>

                                    <Text style={{ color: '#4A90F4', paddingLeft: 10, fontSize: 13, paddingRight: 10, paddingTop: 5, paddingBottom: 5 }}>{item.category_name}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        )
                    })
                }
            </View>
            {
                (this.props.post_image_array.length > 0 && this.props.post_image_array != null)
                    ?
                    <View>
                        <View style={{ backgroundColor: "#fff" }}>
                            <Text style={styles.description}>
                                {this.props.description}
                            </Text>
                        </View>
                        {setBannerImage(this.props.post_image_array[0].post_image)}
                    </View>
                    :
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 1 }}
                        colors={setRandomColors()}
                        style={styles.postView}
                    >
                        <View style={styles.postView}>
                            <Text style={styles.bannerText}>
                                {this.props.description}
                            </Text>
                        </View>
                    </LinearGradient>

            }
        </View>

        <View style={{ height: 40, flexDirection: "row",justifyContent  :"space-between" }}>
            <Button style={{ backgroundColor: "#fff",marginLeft : 20 }} onPress={() => this.likeThPost(this.props.id, this.props.likeStatus == '0' ? 0 : 1, this.props.updateLikeIcons)}>
                <AntDesign name={this.props.likeStatus == '0' ? 'hearto' : 'heart'} color="red" size={20} />
                <Text style = {{color : global.primaryColor}}>{this.state.likeCounts} Likes</Text>
            </Button>
            <Button style={{ backgroundColor: "#fff", flex : 1 }}  onPress={() => this.goToCommentsScreen(this.props.id,this.props.currentNavigation)}>
            <View style = {{alignItems : "center", flexDirection : "row"}}>
                <FontAwesome name="comment-o" size={22} color = {global.primaryColor}/>
                <Text style={{ color: global.primaryColor, paddingLeft : -5 }}> {this.props.total_comment} Comments</Text>
                </View>
            </Button>
            <Button style={{ backgroundColor: "#fff", marginRight : 20 }} onPress={() => saveThePost(this.props.id, this.props.savedStatus == '0' ? 0 : 1, this.props.updateBookmarkIcons)}>
                <FontAwesome name={this.props.savedStatus == '0' ? 'bookmark-o' : 'bookmark'} color={global.text_color_normal} size={20} />
            </Button>
        </View>
    </View>
    )
}
}



