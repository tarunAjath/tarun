import React, { Component } from 'react'
import UserDataHolder from '../../../Login_SignUp/UserDataHolder';
import HelperMethods from '../../../Helper/Methods';
import FeedsListItem from '../components/FeedsListItem'
import { FlatList, View, Dimensions, TouchableWithoutFeedback, Text, Alert } from 'react-native'
import { Button, Card } from "native-base";
import LottieSpinner from '../../../../Helpers/HOC/LottieSpinner';


const LottieLoader = LottieSpinner(View)

export default class TrendingFeeds extends Component {

    state = {
        trending: [],
        search: '',
        isLoadingFeeds: false,
        clickedMenu: false,

    }
    static navigationOptions = {
        header: {
            visible: false,
        }
    }


    showAlertBox(feedId) {
        let index = this.state.trending.findIndex(v => v.id == feedId)
        this.setState({ clickedMenu: true })
    }

    updateBookmarkIcon(id,currentStatus){
        let index = this.state.trending.findIndex(v => v.id == id )
        if(index >  -1){
            let arr = this.state.trending
            arr[index].is_saved = currentStatus == '1' ? '0' : '1'
            this.setState({trending:arr})
        }
        
    }

    updateLikeIcons(id,currentStatus){
        let index = this.state.trending.findIndex(v => v.id == id )
        if(index >  -1){
            let arr = this.state.trending
            arr[index].is_like = currentStatus == '1' ? '0' : '1'
            this.setState({trending:arr})
        }
        
    }

    getTrendingData = () => {
        this.setState({ isLoadingFeeds: true })
        const formData = new FormData();
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append("lat", UserDataHolder.currentLatitude);
        formData.append("long", UserDataHolder.currentLongitude);
        formData.append("categories", UserDataHolder.subscribe_category);
        formData.append("flag_view", '2');
        formData.append("offset", '');
        formData.append("lang", '');
        formData.append("timezone", 'IST');
        HelperMethods.makeNetworkCall_post("feeds_list", formData, (response) => {
            this.setState({ isLoadingFeeds: false })
            if (response.flag == 1) {
                this.setState({ trending: response.data, noFeeds: false })
            } else {
                this.setState({ noFeeds: true })
            }
        })
    }

    renderFeeds = ({ item, index }) => {

        return (
            <FeedsListItem
                key={item.id}
                id={item.id}
                user_id = {item.user_id}
                menuActive={this.state.clickedFeedId == item.id}
                showFeedsUIHandler={(id) => this.showAlertBox(id)}
                title={HelperMethods.Capitalize(item.user_name)}
                subtitle={HelperMethods.Capitalize(item.created_date)}
                description={item.description}
                updateBookmarkIcons={(id,currentStatus) => this.updateBookmarkIcon(id,currentStatus)}
                updateLikeIcons={(id,currentStatus) => this.updateLikeIcons(id,currentStatus)}
                savedStatus={item.is_saved}
                likeStatus={item.is_like}
                image_url={item.user_image}
                categories={item.categories_array}
                post_image_array={item.images}
                tags_array={item.tags_array}
                total_like  = {item.total_like}
                total_comment  = {item.total_comment}
                currentNavigation = {this.props.navigation} 
            />
        )
    }

    render() {
        return (

                <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>
                    <FlatList
                        style={{ flex: 1, marginBottom : 20 , backgroundColor : "#f2f2f2"}}
                        data={this.state.trending}
                        renderItem={this.renderFeeds}
                    />
                    {
                        this.state.noFeeds &&
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <LottieView
                                style={{ height: 100, width: 100, }}
                                source={require('../../../../assets/lottieAnims/noResults.json')}
                                autoPlay
                                loop
                            />
                            <Text>No feeds found</Text>
                        </View>
                    }
                    {
                    this.state.clickedMenu &&
                    <View style={{
                        position: 'absolute', flex: 1,
                        zIndex: 100, width: Dimensions.get('window').width, height: Dimensions.get('window').height, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <TouchableWithoutFeedback onPress={() => this.setState({ clickedMenu: false })}>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', width: '100%', height: '100%' }}>

                            </View>
                        </TouchableWithoutFeedback>
                        <Card style={{ width: Dimensions.get('window').width, height: 200, position: 'absolute', bottom: Dimensions.get('window').height / 2 }}>
                            <Button onPress={this.onShare}
                                transparent
                            >
                                <Text style={{ color: 'black' }}>   Share</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Follow this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Hide this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Report</Text>
                            </Button>
                        </Card>
                    </View>

                }
                </LottieLoader>
        );
    }
}

const styles = {
    overlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'center',

    }
}


