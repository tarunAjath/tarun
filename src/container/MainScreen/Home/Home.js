import React, { Component } from 'react'
import UserDataHolder from '../../Login_SignUp/UserDataHolder';
import {Container,Tab,Tabs,Item,Input,Card} from "native-base";
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner';
import AppBar from './components/AppBar';
import Feeds from './Tabs/Feeds'
import TrendingFeeds from './Tabs/TrendingFeeds'
import '../../Helper/Global'
import {Platform,LayoutAnimation,UIManager,Animated,TouchableWithoutFeedback,View,BackHandler,Image,TouchableOpacity,Easing,Keyboard} from 'react-native'
import Search from './components/Search/Search';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import * as Animatable from 'react-native-animatable'
import Filter from './components/Search/Filter';

let searchWidthIncrease = 55
let searchAnimationSpeed = 600

const ContainerWithLottie = LottieSpinner(Container);
export default class Home extends Component {

    state = {
        searchInput:'',
        isLoadingFeeds : false,
        clickedFeed: false,
        isSearchActive:false,
        getResults:false,
        showFilters:false,
        refreshFilterCats:false,
    }

    static navigationOptions = {
        header:null
    }

    componentWillMount() {
        this.setState({ isLoadingFeeds: true })
        global.navigation = this.props.navigation

        Platform.OS == 'android' && UIManager.setLayoutAnimationEnabledExperimental(true)
        
        this.searchInput = null;
        BackHandler.addEventListener('hardwareBackPress',this.backHandler)
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.searchWidth = new Animated.Value(250)
        this.setState({ isLoadingFeeds: true })
        global.navigation = this.props.navigation
        this.props.navigation.addListener('willFocus',this.willFocus)
    }

    willFocus = () => {
        this.setState({refreshFilterCats:true},()=>{
            this.setState({refreshFilterCats:false})
        })

    }
    backHandler = () => {
        if(this.state.isSearchActive){
            this.hideSearch()
            return true
        }
    }

    _keyboardDidShow () {
    }

    _keyboardDidHide = () => {
      if(this.state.isSearchActive && !this.state.getResults){
        this.setState({getResults:false})
      }
    }

  componentWillUnmount(){
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
      BackHandler.removeEventListener('hardwareBackPress',this.backHandler)
  }


    findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const location = JSON.stringify(position);
                UserDataHolder.currentLatitude = position.coords.latitude,
                    UserDataHolder.currentLongitude = position.coords.longitude,
                    this.setState({ location });
                    this.getfeedsData()
            },
            // error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    navigateNotifications = () => {
        this.props.navigation.navigate('notifications')
    }

    navigateLocationSelector = () => {
        this.props.navigation.navigate('locationPicker')
    }

    search = () => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
        this.setState({getResults:true,showFilters:false}, () => {
            this.setState({getResults:false})
        })
    }

    animateSearch_active(){
        Animated.timing(this.searchWidth,{
            toValue:this.searchWidth.__getValue()+searchWidthIncrease,
            duration:searchAnimationSpeed,
            easing:Easing.out(Easing.quad),
        }).start()
    }

    animateSearch_unActive(){
        Animated.timing(this.searchWidth,{
            toValue:this.searchWidth.__getValue() - searchWidthIncrease,
            duration:searchAnimationSpeed,
            easing:Easing.out(Easing.quad),
        }).start()
        this.searchInput._root.blur()
    }

    showSearch(){
        this.animateSearch_active()
        this.setState({isSearchActive:true,getResults:false},()=>{
        })
    }

    hideSearch = () => {
        this.animateSearch_unActive()
        this.setState({isSearchActive:false},()=>{
        })
        this.clearSearchInput()
    }

    clearSearchInput(){
        this.setState({searchInput:''})
    }
    
    renderHeaderLeft(){
        if(this.state.isSearchActive){
            return(
                <Animatable.View delay={searchAnimationSpeed} duration={400} animation='zoomIn' style={{left:10}}>
                    <TouchableOpacity onPress={this.hideSearch}>
                        <AntDesign name='arrowleft' size={22} color={global.text_color_normal} />
                    </TouchableOpacity>
                </Animatable.View>
            )
        } else {
            return(
                    <Image
                    style={{ height: 31, width: 31,left:10 }}
                    source={require("../../img/icon_nimmio.png")}
                    />
            )
        }
    }

    toggleFilter(){
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
        this.setState({showFilters:!this.state.showFilters})
    }
    renderHeaderRight(){
        if(this.state.isSearchActive){
            return(
                <Animatable.View delay={searchAnimationSpeed-50} duration={searchAnimationSpeed} animation='fadeInRight' style={{flexDirection:'row',alignItems:'center'}}>
                    <TouchableOpacity style={styles.icons} onPress={() => this.toggleFilter()}>
                        <MaterialCommunityIcons name={this.state.showFilters ? 'filter' : 'filter-outline'  } size={22} color={global.text_color_normal}  />
                    </TouchableOpacity>
                    
                    {
                        this.state.searchInput.length > 0 &&
                    <TouchableOpacity onPress={()=>{ LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);this.clearSearchInput()}} style={styles.icons}>
                            <AntDesign name='close' size={22}  color={global.text_color_normal} />
                    </TouchableOpacity>                    
                    }
                </Animatable.View>
            )
        } else {
            return(
                <Animatable.View duration={800} animation='fadeIn' style={{flexDirection:'row',alignItems:'center'}}>
                        <TouchableOpacity onPress={()=>this.navigateLocationSelector()} style={styles.icons}>
                            <SimpleLineIcons color={global.text_color_normal} name='location-pin' size={20} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={()=>this.navigateNotifications()} style={styles.icons}>
                            <SimpleLineIcons color={global.text_color_normal} name='bell' size={20} />
                        </TouchableOpacity>
                </Animatable.View>
            )
        }
    }

    render() {
        return (
            <ContainerWithLottie isApiCall={false} >
                <View style={styles.container}>
                {
                    this.renderHeaderLeft()
                }
                
                    <Animated.View style={{flex:0.9,}}>
                        <Item 

                        style={{width: this.searchWidth,borderBottomColor:global.text_color_normal,bottom:7}}
                        fixedLabel>
                            <Input 
                            ref={input => this.searchInput = input}
                            onFocus={() => this.showSearch()}
                            onSubmitEditing={this.search}
                            style={{paddingBottom:0}}
                            placeholder='Search'
                            placeholderTextColor={global.text_color_light}
                            value={this.state.searchInput} 
                            onChangeText={(text)=> { this.state.searchInput.length == 0 ? LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut) : null; this.setState({searchInput:text})} } />
                        </Item>
                    </Animated.View>
                   {this.renderHeaderRight()}
                </View>

                { (this.state.showFilters && this.state.isSearchActive ) && 
                <Filter 
                refreshCats={this.state.refreshFilterCats}
                navigation={this.props.navigation}
                />
            }
                {
                    this.state.isSearchActive ?
                    <Search 
                    query={this.state.searchInput}
                    getResults={this.state.getResults} />
                    :
                <Tabs
                tabBarUnderlineStyle={{borderBottomWidth:0.4}}
                tabContainerStyle={{
                    elevation:0,
                    marginTop:10
                }}
                tabBarUnderlineStyle={{backgroundColor:global.primaryColor}}>
                    <Tab
                    textStyle={{color:global.text_color_light}}
                    activeTextStyle={styles.tabs_text}
                    tabStyle={styles.tabs}
                    activeTabStyle={styles.tabs}
                    heading="Feeds">
                        <Feeds navigation = {this.props.navigation} />
                    </Tab>

                    <Tab 
                    textStyle={{color:global.text_color_light}}
                    activeTextStyle={styles.tabs_text}
                    tabStyle={styles.tabs}
                    activeTabStyle={styles.tabs}
                    heading="Trending">
                        <TrendingFeeds navigation = {this.props.navigation}  />
                    </Tab>
                </Tabs>
                }
              
            </ContainerWithLottie>
        );
    }
}

const styles = {
    
    container:{
        width: global.deviceWidth,
        flexDirection: 'row', 
        marginTop: Platform.OS == 'ios' ? 30 : 0, 
        alignItems:'center',
        justifyContent:'space-between',
        height: 50 ,

    },
    icons:{
        marginLeft:5,
        marginRight:5
    },


    tabs_text:{
        color:global.text_color_normal,
        fontFamily:global.fontFamily_Regular
    },

    tabs:{
        backgroundColor:'#fff',
        borderBottomWidth:0.4,
        borderBottomColor:global.text_color_light
    },

}