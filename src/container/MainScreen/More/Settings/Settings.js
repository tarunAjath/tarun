import React, { Component } from "react";
import { View, Text, ScrollView, Switch,Alert,TouchableOpacity, Modal } from "react-native";
import { Button,Item,Input } from "native-base";
import Overlay from "react-native-modal-overlay";


export default class Settings extends Component {
  state = {
    reportVisible: false,
    feedbackModal: false
  };

  onReportClose = () => this.setState({ reportVisible: false });
    
  onFeedClose = () => this.setState({ feedbackModal: false });

  render() {
    

    return (
      <View style = {{paddingBottom : 30}} >

        <ScrollView >
          <View>
            <View style={{ marginTop: 10 }}>
              <View style={styles.line} />
            </View>
            <Text style={{ margin: 10, fontSize: 20, fontWeight: 'bold', color: "black" }}> Account </Text>

            <View style={styles.line_} />
          </View>

          <View style={{ margin: 10 }}>
            <View >
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("editemail")}>
                <Text style={{ color: 'black', fontSize: 18, }}> Edit your email address </Text>
              </Button>
            </View>

            <View>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("changepassword")}>
                <Text style={{ color: 'black', fontSize: 18, }}> Change your password </Text>
              </Button>
            </View>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("mobilenumber")}>
              <Text style={{ color: 'black', fontSize: 18, }}> Change your Mobile number </Text>
            </Button>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("userid")}>
              <Text style={{ color: 'black', fontSize: 18, }}> Change User ID</Text>
            </Button>
            <Button
            transparent
        onPress={showAlert}
        >
        <Text style={{ marginLeft:5,color: 'black', fontSize: 18, }}>
            Delete
        </Text>
        </Button>
      

          </View>

          <View>
            <View >
              <View style={styles.line} />
            </View>
            <Text style={{ margin:10, fontSize: 20, fontWeight: 'bold', color: "black"   }}> Notifications </Text>
            <View style={styles.line_} />
          </View>
          <View style={{ margin: 15}}>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
              <Text style={{ color: 'black', fontSize:18 }}>Push Notifications</Text>
              <Switch 
                value={true}
                activeText={'On'}
                inActiveText={'Off'}
              />
            </View>
            <View style={{flexDirection:'row',marginTop:10, justifyContent:'space-between'}}>
              <Text style={{fontSize:18, color: 'black' }}>Notification Sounds</Text>
              <Switch value={true}
                activeText={'On'}
                inActiveText={'Off'}
              />
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:10}}>
              <Text style={{ color: 'black', fontSize:18  }}>Like Notifications</Text>
              <Switch value={true}
                activeText={'On'}
                inActiveText={'Off'}
              />
            </View>
          </View>
          <View>
            <View style={{ marginTop: 10 }}>
              <View style={styles.line} />
            </View>
            <Text style={{  margin: 15, fontSize: 20, fontWeight: 'bold', color: "black" }}>Privacy </Text>
            <View style={styles.line_} />
          </View>
          <View style={{ margin: 10 }}>
            <View style={{flexDirection:'row', justifyContent:'space-between',marginTop:12}}>
              <Text style={{ color: 'black', fontSize:18 }}> Location</Text>
              <Switch value={true}
                activeText={'On'}
                inActiveText={'Off'}
              />
            </View>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("blocked")}>
              <Text style={{ color: 'black' , fontSize:18 ,marginTop:12 }}> Show Blocked users </Text>
            </Button>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("hiddenfeeds")}>
              <Text style={{ color: 'black' , fontSize:18}}> View hidden feeds </Text>
            </Button>
          </View>
          <View>
            <View style={{ marginTop: 10 }}>
              <View style={styles.line} />
            </View>
            <Text style={{ margin: 10, fontSize: 20, fontWeight: 'bold', color: "black" }}> About and help </Text>
            <View style={styles.line_} />
          </View>
          <View style={{ margin: 10 }} >
             
          <Button
              transparent
              onPress={() => this.setState({ reportVisible: true })}>
              <Text style={{ color: 'black', fontSize:18 }}> Report Problem </Text>
            </Button> 
             <Overlay
          visible={this.state.reportVisible}
          onClose={this.onReportClose}
          closeOnTouchOutside
          animationType="zoomIn"
          containerStyle={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
          childrenWrapperStyle={{ backgroundColor: "#fff" }}
          animationDuration={500}
        >
          {hideModal => (

            <ScrollView>
              <View>
                <View style={{ flex: 1, backgroundColor: '#4A90F4', height: 10 }} />
                <View style={{ margin: 20 }}>
                  <Text style={{ fontSize: 24, fontWeight: "bold", color: "#000" }}>Report Problem</Text>
                </View>
                <View style={{ marginRight: 25, marginLeft: 25 }}>
                  <Item style={{ backgroundColor: '#fff', height: 80 }}>
                    <Input
                      style={{ alignSelf: 'flex-start' }}
                      placeholder="Type your message here"
                       />
                  </Item>
                </View>

                <View>
                  <Button
                    rounded
                    style={styles.btn}
                  >
                    <Text style={styles.btnText}>Submit</Text>
                  </Button>

                  <Button
                    rounded
                    style={styles.btn}
                    onPress={hideModal}
                  >
                    <Text style={styles.btnText}>Close</Text>
                  </Button>

                </View>

              </View>
            </ScrollView>
          )}
        </Overlay>







            <Button
              transparent
              onPress={() => this.props.navigation.navigate("contactus")}>
              <Text style={{ color: 'black',fontSize:18 }}> Contact us </Text>
            </Button>
           





            <Button
              transparent
              onPress={() => this.setState({ feedbackModal: true })}>
              <Text style={{ color: 'black', fontSize:18 }}> Send Feedback </Text>
            </Button> 
             <Overlay
          visible={this.state.feedbackModal}
          onClose={this.onFeedClose}
          closeOnTouchOutside
          animationType="zoomIn"
          containerStyle={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
          childrenWrapperStyle={{ backgroundColor: "#fff" }}
          animationDuration={500}
        >
          {hideFeedModal => (

            <ScrollView>
              <View>
                <View style={{ flex: 1, backgroundColor: '#4A90F4', height: 10 }} />
                <View style={{ margin: 20 }}>
                  <Text style={{ fontSize: 24, fontWeight: "bold", color: "#000" }}>Send Feedback</Text>
                </View>
                <View style={{ marginRight: 25, marginLeft: 25 }}>
                  <Item style={{ backgroundColor: '#fff', height: 80 }}>
                    <Input
                      style={{ alignSelf: 'flex-start' }}
                      placeholder="Type your message here"
                       />
                  </Item>
                </View>

                <View>
                  <Button
                    rounded
                    style={styles.btn}
                  >
                    <Text style={styles.btnText}>Submit</Text>
                  </Button>

                  <Button
                    rounded
                    style={styles.btn}
                    onPress={hideFeedModal}
                  >
                    <Text style={styles.btnText}>Close</Text>
                  </Button>

                </View>

              </View>
            </ScrollView>
          )}
        </Overlay>






            <Button
              transparent
              onPress={() => this.props.navigation.navigate("terms")}>
              <Text style={{ color: 'black',fontSize:18 }}> Terms and conditions </Text>
            </Button>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("licences")}>
              <Text style={{ color: 'black',fontSize:18 }}> Licences </Text>
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}



const showAlert=()=>{
  Alert.alert(
      'Nimmio',
      'Would you like to delete this account?',
      [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Yes', onPress: () => console.log('OK Pressed')},
      ],

      {cancelable: false},
    );
  
}









const styles = {
  line: {
    alignSelf: "center",
    position: "absolute",
    borderBottomColor: "rgba(112, 112, 112 , 0.3)",
    borderBottomWidth: 1,
    height: "100%",
    width: "100%"
  },
  line_: {
    padding: 20,
    alignSelf: "center",
    position: "absolute",
    borderBottomColor: "rgba(112, 112, 112 , 0.3)",
    borderBottomWidth: 1,
    height: "100%",
    width: "100%"
  },
  btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
  },
  btn: {
    alignSelf: 'flex-end',
    backgroundColor: "#4A90F4",
    marginTop: 25,
    marginRight: 25,
    height: 40,
  }
};
