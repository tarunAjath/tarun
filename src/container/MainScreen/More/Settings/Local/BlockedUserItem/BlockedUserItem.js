import React from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import HelperMethods from '../../../../../Helper/Methods'
import { Container, Header, Content, Button, Icon, Text } from 'native-base';

const placeHolderImage = require('../../../../../../assets/images/userPlaceholder.png')


const setUserImage = (userImageURL) => {

    if (userImageURL) {
        return <Image source={{ uri: userImageURL }} style={styles.photo} />
    } else {
        return <Image source={placeHolderImage} style={styles.photo} />

    }
}


const BlockedUserItem = ({ name, user_id, isFollowing, image_url, currentNavigation }) => (

    <View>

        <View style={styles.topContainer}>
            <View>
                {setUserImage(image_url)}
            </View>
            <View style={{ marginLeft: 16, marginRight: 20, flex: 1, flexDirection: "row" }}>
                <Text style={{ color: global.primaryColor }}>{name}</Text>


            </View>
        </View>
    </View>
);


const goToOtherUserProfiles = (id, currentNavigation) => {
    HelperMethods.navigateToScreen(currentNavigation, { 'id': id }, 'otherUserProfile')
}




const styles = StyleSheet.create({
    photo: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
    },
    topContainer: {
        flexDirection: 'row',
        padding: 16,
        backgroundColor: '#FFF',
        elevation: 2,
    },
})
export default BlockedUserItem;
