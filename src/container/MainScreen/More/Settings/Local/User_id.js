import React, { Component } from 'react';
import { View, Text, ScrollView,Switch , Image} from 'react-native';
import { Button , Item, Input} from 'native-base';

export default class User_id extends Component {
    render () {
        return (
            <ScrollView>
                  <Image
                style={styles.icon}
                source={require("../../../../../img/icon_head.png")}
                />
             <Text style={styles.Edit}> Change User ID</Text>
            <View style={{marginLeft :70,marginRight:70, marginTop:10}}>
            <Item style={{  color: '#4A90F4'}}>
             <Input
             
              placeholder="User ID/Username"
            />
          </Item>
          </View>
               
               <Button
               rounded
               
                style={styles.btn} >
              <Text style={styles.btnText} > Save </Text>
              </ Button>
            </ScrollView>
        );
    }
}
const styles ={
    Edit: {
      color: '#4A90F4',
      fontWeight: 'bold',
      fontSize: 20,
      alignSelf: 'center',
      marginTop: 30
},
icon: {
  alignSelf: "center",
  marginTop: 45,
    width: 80,
   height: 80,
    flex:1,
    resizeMode: 'contain' 
},
    btnText: {
      color: "white",
      paddingRight: 51,
      paddingLeft: 51,
      fontSize: 14,
      fontFamily: "Gill Sans"
    },
    btn: {
      alignSelf: "center",
      backgroundColor: "#4A90F4",
      marginTop: 35, 
      height: 40,
    }
}