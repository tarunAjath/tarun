import React, { Component } from "react";
import { Text, View,ScrollView } from "react-native";
import { Item, Input, Button } from "native-base";

export default class Report extends Component {
  render() {
    return (

<ScrollView>
      <View>
        <View style={{flex:1,backgroundColor:'#4A90F4', marginLeft:5,marginRight:5, height:10}}/>
        <View style={{margin:20}}>
            <Text style={{fontSize:24, fontWeight:"bold", color:"#000"}}>Report Problem</Text>
        </View>
        <View style={{marginRight:25, marginLeft:25}}>
        <Item style={{ backgroundColor: '#fff', height: 80 }}>
                        <Input
                            style={{ alignSelf: 'flex-start' }}
                            placeholder="Type your message here"
                            placeholderborderColor='red'/>
                            </Item>
        </View>
        
        <View>
        <Button
              rounded
              style={styles.btn}
            >
              <Text style={styles.btnText}>Submit</Text>
            </Button>

            <Button
              rounded
              style={styles.btn}
              
            >
              <Text style={styles.btnText}>Close</Text>
            </Button>
            
        </View>

      </View>
      </ScrollView>
    );
  }
}
const styles={
btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
    fontFamily: "Gill Sans"
  },
  btn: {
    alignSelf: 'flex-end',
    backgroundColor: "#4A90F4",
    marginTop: 25,
    marginRight:25,
    height: 40,
  },
}