import React, { Component } from "react";
import { Text, View,ScrollView } from "react-native";
import { Item, Input, Button } from "native-base";
import Overlay from "react-native-modal-overlay";

export default class Feedback extends Component {
  state = {
   feedbackModal: false
  };

  onClose = () => this.setState({ feedbackModal: false });
  render() {
    return (

<ScrollView>
      <View>
      <Button
              transparent
              onPress={() => this.setState({ feedbackModal: true })}>
              <Text style={{ color: 'black', fontSize:18 }}> Send Feedback </Text>
            </Button> 
             <Overlay
          visible={this.state.feedbackModal}
          onClose={this.onClose}
          closeOnTouchOutside
          animationType="zoomIn"
          containerStyle={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
          childrenWrapperStyle={{ backgroundColor: "#fff" }}
          animationDuration={500}
        >
          {hideModal => (

            <ScrollView>
              <View>
                <View style={{ flex: 1, backgroundColor: '#4A90F4', height: 10 }} />
                <View style={{ margin: 20 }}>
                  <Text style={{ fontSize: 24, fontWeight: "bold", color: "#000" }}>Send Feedback</Text>
                </View>
                <View style={{ marginRight: 25, marginLeft: 25 }}>
                  <Item style={{ backgroundColor: '#fff', height: 80 }}>
                    <Input
                      style={{ alignSelf: 'flex-start' }}
                      placeholder="Type your message here"
                       />
                  </Item>
                </View>

                <View>
                  <Button
                    rounded
                    style={styles.btn}
                  >
                    <Text style={styles.btnText}>Submit</Text>
                  </Button>

                  <Button
                    rounded
                    style={styles.btn}
                    onPress={hideModal}
                  >
                    <Text style={styles.btnText}>Close</Text>
                  </Button>

                </View>

              </View>
            </ScrollView>
          )}
        </Overlay>

      </View>
      </ScrollView>
    );
  }
}
const styles={
btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
    fontFamily: "Gill Sans"
  },
  btn: {
    alignSelf: 'flex-end',
    backgroundColor: "#4A90F4",
    marginTop: 25,
    marginRight:25,
    height: 40,
  },
}