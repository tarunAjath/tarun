import React, { Component } from 'react';
import { View, Text, ScrollView,TextInput} from 'react-native';
import { Button , Item, Input} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';


export default class Mobile_num extends Component {
    render() {
        let data = [{
            value: 'Japan',
          }, {
            value: 'America',
          }, {
            value: 'India',
          }];
        return (
            <View style={{flex: 1 , backgroundColor:'#F4F4F4'}}>
            <ScrollView >
           <View style={{margin:10 , backgroundColor:'white'}}>
            <View style={{margin:20 }}> 
                <Text style={{color:'black'}}>
                    Nimmio will send a one time SMS message to verify your number.
                </Text>
            </View>
            </View>
            
            <View style={{margin:10 ,marginTop:6, backgroundColor:'white'}}>
            <View style={{margin:20 }}>
                <Text style={{color:'black'}}> Please confirm your country code and enter your phone number.</Text>
            </View>
            
            <View style={{ margin:20 , marginTop:15}}>
            <Dropdown
            
            data={data} />
            </View>
            <View style={styles.MainContainer}>
            <TextInput
 
         placeholder="Text Input For Numeric Value"
 
         style={styles.TextInputStyle}
 
         keyboardType={'numeric'}
 
       />
       </View>
            <View style={{marginBottom:30}}>
            <Button style={styles.btn} >
              <Text style={styles.btnText} > Continue </Text>
              </ Button>
              </View>

            </View>

            </ScrollView>
            </View>
        );
    }
}

const styles ={
    btnText: {
        color: "white",
        paddingRight: 80,
        paddingLeft: 80
      },
      btn: {
        alignSelf: "center",
        backgroundColor: "#4A90F4",
        borderRadius: 50,
        marginTop: 10,
        height: 35,
      },
      MainContainer :{
 
        justifyContent: 'center',
        flex:1,
        margin: 10
        },
         
        TextInputStyle: {
         
        textAlign: 'center',
         
        }
         
        
}