import React, { Component } from 'react';
import { View, Text, ScrollView,Image} from 'react-native';
import { Button , Item ,Input} from 'native-base';

export default class Contact_us extends Component {
    render() {
        return(
            <ScrollView>
                  <Image
                style={styles.icon}
                source={require("../../../../../img/icon_head.png")}
                />
             <Text style={styles.Edit}> Contact us</Text>
            <View style={{margin :70, marginTop:10}}>
            <Item style={{  color: '#4A90F4'}}>
             <Input
              placeholder="Full Name"
            />
          </Item>
          <Item style={{  color: '#4A90F4'}}>
             <Input
              placeholder="Subject"
            />
          </Item>
          <Item style={{ height:60, color: '#4A90F4'}}>
             <Input
              placeholder="Message"
            />
          </Item>
          <Button 
          rounded
          style={styles.btn} >
              <Text style={styles.btnText} >Save </Text>
              </ Button>
          </View>
          </ScrollView>
        );
    }
}

const styles ={
    Edit: {
      color: '#4A90F4',
      fontWeight: 'bold',
      fontSize: 20,
      alignSelf: 'center',
      marginTop: 25
},
icon: {
  alignSelf: "center",
  marginTop: 45,
    width: 80,
   height: 80,
    flex:1,
    resizeMode: 'contain' 
},
btnText: {
  color: "white",
  paddingRight: 51,
  paddingLeft: 51,
  fontSize: 14,
  fontFamily: "Gill Sans"
},
btn: {
  alignSelf: "center",
  backgroundColor: "#4A90F4",
  marginTop: 35, 
  height: 40
 
}
}