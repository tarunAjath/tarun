import React, { Component } from 'react'
import { Text, View, Image, StyleSheet,FlatList } from 'react-native'
import UserDataHolder from "../../../../Login_SignUp/UserDataHolder"
import HelperMethods from "../../../../Helper/Methods"
import LottieView from 'lottie-react-native'
import LottieSpinner from '../../../../../Helpers/HOC/LottieSpinner';
import BlockedUserItem from '../../Settings/Local/BlockedUserItem/BlockedUserItem';

const LottieLoader = LottieSpinner(View)

const placeHolderImage = require('../../../../../assets/images/userPlaceholder.png')

const setUserImage = (userImageURL) => {

  if (userImageURL) {
    return <Image source={{ uri: userImageURL }} style={styles.photo} />
  } else {
    return <Image source={placeHolderImage} style={styles.photo} />

  }
}


export default class Blocked_users extends Component {

  state = {
    myFollowers: [],
    isLoadingFeeds: false,
    noFeeds: false,
  }

  static navigationOptions =
  {
   headerTintColor: global.primaryColor,
  };

  componentDidMount() {
    this.getBlockUsersList()
  }


  getBlockUsersList = () => {

    this.setState({ isLoadingFeeds: true })
    const formData = new FormData();
    formData.append("user_id", UserDataHolder.userId);
    formData.append("access_token", UserDataHolder.access_token);
    formData.append("flag_view", '0');
    formData.append("offset", '0');

    HelperMethods.makeNetworkCall_post("manage_block_user", formData, (response) => {
      this.setState({ isLoadingFeeds: false })
      if (response.flag == 1) {
        this.setState({ myFollowers: response.data, noFeeds: false })
      } else {
        this.setState({ noFeeds: true })
      }
    })
  }



  renderFeeds = ({ item, index }) => {

    return (
        <BlockedUserItem
            key={item.id}
            name = {item.name}
            user_id = {item.id}
            isFollowing = ""
            image_url = {item.profile_image}
            currentNavigation = {this.props.navigation} 
        />
    )
}

  render() {
    return (
      <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>

        <View style={{ flex: 1, }}>
        {
            <FlatList
              style={{ marginBottom: 20, }}
              data={this.state.myFollowers}
              renderItem={this.renderFeeds}
            />
          
        }
        {
          this.state.noFeeds &&
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <LottieView
              style={{ height: 100, width: 100, }}
              source={require('../../../../../assets/lottieAnims/noResults.json')}
              autoPlay
              loop
            />
            <Text>No one following you</Text>
          </View>
        }
      </View>
      </LottieLoader>

    )
  }
}


const styles = StyleSheet.create({

  photo: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
  })