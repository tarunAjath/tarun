import React, { Component } from 'react';
import { View, Text, ScrollView, Switch, Image } from 'react-native';
import { Button, Item, Input } from 'native-base';


export default class Change_password extends Component {
  render() {
    return (
      <ScrollView>
        <Image
          style={styles.icon}
          source={require("./../../../../../img/icon_head.png")}
        />
        <Text style={styles.Change}> Change your password</Text>
        <View style={{ marginLeft: 75, marginRight: 75, marginTop: 20 }}>
          <Item style={{ color: '#4A90F4' }}>
            <Input

              placeholder="Old Password"
            />
          </Item>
          <Item style={{ color: '#4A90F4', marginTop: 15 }}>
            <Input

              placeholder="New Password"
            />
          </Item>
          <Item style={{ color: '#4A90F4', marginTop: 15 }}>
            <Input

              placeholder="Confirm Password"
            />
          </Item>
        </View>
        <View style={{ marginTop: 25 }}>
          <Button
            rounded
            style={styles.btn} >
            <Text style={styles.btnText} >Change Password</Text>
          </ Button>
        </View>
      </ScrollView>
    );
  }
}
const styles = {
  Change: {
    color: '#4A90F4',
    fontWeight: 'bold',
    fontSize: 20,
    marginTop: 30,
    alignSelf: 'center'
  },
  btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
    fontFamily: "Gill Sans"
  },
  btn: {
    alignSelf: "center",
    backgroundColor: "#4A90F4",
    marginTop: 15,
    height: 40,
  },
  icon: {
    alignSelf: "center",
    marginTop: 30,
    width: 80,
    height: 80,
    flex: 1,
    resizeMode: 'contain'
  }
}
