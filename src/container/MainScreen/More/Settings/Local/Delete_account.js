import React, { Component } from 'react'
import { Text, View, Alert } from 'react-native'
import { Button } from 'native-base';

export default class Delete_account extends Component {
   
  render() {

    return (
      <View>
        <Button
        onPress={showAlert}
        >
        <Text>
            Delete
        </Text>
        </Button>
      </View>
    )
  }
}
const showAlert=()=>{
    Alert.alert(
        'Nimmio',
        'Would you like to delete this account?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => console.log('OK Pressed')},
        ],

        {cancelable: false},
      );
    
}
