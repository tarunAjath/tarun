import React, { Component } from 'react';
import { View, Text, ScrollView, Image, Keyboard, AsyncStorage } from 'react-native';
import { Button, Item, Input } from 'native-base';
import UserDataHolder from '../../../../Login_SignUp/UserDataHolder'
import '../../../../Helper/Global'
import ValidationLogics from '../../../../Helper/InputValidations_logic'
import HelperMethods from '../../../../Helper/Methods'


export default class Edit_email extends Component {

  state = {
    user_email: UserDataHolder.user_email,
    isNetworkCall: false
  }
  navigateSetEmail = () => {
    if (this.state.user_email.length == 0) {
      alert('Please enter your registered email')
    } else {
      ValidationLogics.validateEmail(this.state.user_email, (valid) => {
        if (!valid) {
          alert('Please enter valid email')
          return false
        } else {
          Keyboard.dismiss()
          this.setState({ isNetworkCall: true })

          const formData = new FormData()
          formData.append("username_email", this.state.user_email)
          formData.append("access_token", UserDataHolder.access_token)
          formData.append("user_id", UserDataHolder.data.id)
          formData.append("flag_view", 3)
          formData.append("timezone", UserDataHolder.data.timezone)


          HelperMethods.makeNetworkCall_post("save_user_profile", formData, response => {
            this.setState({ isNetworkCall: false })
            if (response.flag == 1) {
              //alert(response.msg)
              HelperMethods.snackbar("Email ID updated succesfully", 'OK', () => { })
              //let userId = response.data.user_details.id
              // this.props.navigation.navigate('Edit_email', { email: this.state.user_email, user_id: userId })
            } else {
              alert(response.msg)
            }
          })
        }
      })
    }
  }

  render() {
    return (
      <ScrollView>
        <Image
          style={styles.icon}
          source={require("../../../../../img/icon_head.png")}
        />
        <Text style={styles.Edit}>Edit your email address</Text>

        <View style={{ marginTop: 10, marginLeft: 75, marginRight: 75 }}>
          <Item style={{ color: '#4A90F4' }}>
            <Input
              value={this.state.user_email}
              onChangeText={text => this.setState({ user_email: text })}
            />
          </Item>
        </View>

        <View style={{ marginTop: 15 }} >
          <Button
            rounded
            style={styles.btn}
            onPress={this.navigateSetEmail.bind(this)}
          >

            <Text style={styles.btnText} > Update </Text>
          </ Button>
        </View>

      </ScrollView>
    );
  }
}
const styles = {
  Edit: {
    color: '#4A90F4',
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center',
    marginTop: 25
  },
  icon: {
    alignSelf: "center",
    marginTop: 45,
    width: 80,
    height: 80,
    flex: 1,
    resizeMode: 'contain'
  },
  btnText: {
    color: "white",
    paddingRight: 51,
    paddingLeft: 51,
    fontSize: 14,
    fontFamily: "Gill Sans"
  },
  btn: {
    alignSelf: "center",
    backgroundColor: "#4A90F4",
    marginTop: 15,
    height: 40

  }
}