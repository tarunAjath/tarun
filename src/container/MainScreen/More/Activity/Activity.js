import React, { Component } from 'react';
import { View, StyleSheet, Text, Dimensions, TouchableWithoutFeedback, FlatList } from 'react-native';
import UserDataHolder from '../../../Login_SignUp/UserDataHolder';
import HelperMethods from '../../../Helper/Methods';
import FeedsListItem from '../../Home/components/FeedsListItem'
import LottieSpinner from '../../../../Helpers/HOC/LottieSpinner'
import LottieView from 'lottie-react-native'
import { Button, Card } from "native-base";



const LottieLoader = LottieSpinner(View)

export default class Activity extends Component {

    state = {
        myfeeds: [],
        isLoadingFeeds: false,
        clickedFeed: false,
        noFeeds: false
    }

    static navigationOptions =
        {
            headerTintColor: global.primaryColor,
        };


    componentWillMount() {
        global.navigation = this.props.navigation
        this.findCoordinates()

    }


    showAlertBox(feedId) {
        let index = this.state.feeds.findIndex(v => v.id == feedId)
        this.setState({ clickedMenu: true })
    }

    findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const location = JSON.stringify(position);
                UserDataHolder.currentLatitude = position.coords.latitude,
                    UserDataHolder.currentLongitude = position.coords.longitude,
                    this.setState({ location });
                this.getMyActivityData()

            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };


    getMyActivityData = () => {

        this.setState({ isLoadingFeeds: true })
        const formData = new FormData();
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append("profile_id", UserDataHolder.userId);


        HelperMethods.makeNetworkCall_post("user_activity", formData, (response) => {
            this.setState({ isLoadingFeeds: false })
            if (response.flag == 1) {
                this.setState({ myfeeds: response.data, noFeeds: false })
            } else {
                this.setState({ noFeeds: true })
            }
        })
    }



    renderFeeds = ({ item, index }) => {
        return (
            <FeedsListItem
                key={item.id}
                id={item.id}
                user_id = {item.user_id}
                menuActive={this.state.clickedFeedId == item.id}
                showFeedsUIHandler={(id) => this.showAlertBox(id)}
                title={HelperMethods.Capitalize(item.user_name)}
                subtitle={HelperMethods.Capitalize(item.created_date)}
                description={item.description}
                image_url={item.user_image}
                categories={item.categories_array}
                post_image_array={item.images}
                tags_array={item.tags_array}
                currentNavigation = {this.props.navigation} 
            />
        )
    }

    render() {
        return (

            <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>

                <FlatList
                    style={{ marginBottom: 20 }}
                    data={this.state.myfeeds}
                    renderItem={this.renderFeeds}
                />
                {
                    this.state.noFeeds &&
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <LottieView
                            style={{ height: 100, width: 100, }}
                            source={require('../../../../assets/lottieAnims/noResults.json')}
                            autoPlay
                            loop
                        />
                        <Text>No feeds found</Text>
                    </View>
                }
                {
                    this.state.clickedMenu &&
                    <View style={{
                        position: 'absolute', flex: 1,
                        zIndex: 100, width: Dimensions.get('window').width, height: Dimensions.get('window').height, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <TouchableWithoutFeedback onPress={() => this.setState({ clickedMenu: false })}>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', width: '100%', height: '100%' }}>

                            </View>
                        </TouchableWithoutFeedback>
                        <Card style={{ width: Dimensions.get('window').width, height: 200, position: 'absolute', bottom: Dimensions.get('window').height / 2 }}>
                            <Button onPress={this.onShare}
                                transparent
                            >
                                <Text style={{ color: 'black' }}>   Share</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Follow this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Hide this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Report</Text>
                            </Button>
                        </Card>
                    </View>

                }
            </LottieLoader>
        );
    }
}

const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
});