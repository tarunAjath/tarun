import React, { Component } from "react";
import { UIManager, BackHandler,AsyncStorage,View } from "react-native";
import ActionButton from "react-native-action-button";
import '../../Helper/Global'
import { Icon } from 'native-base'
import FontAwesome from "react-native-vector-icons/FontAwesome";
import NavigationService from "../../Helper/NavigationService";


export class More extends Component {
 
 
  state = {
    isLoggedIn: 'false',
  }

  componentWillMount() {
    this.checkIfLoggedin()
    BackHandler.addEventListener('hardwareBackPress', this.backHandler)
  }



  checkIfLoggedin = async () => {
     await AsyncStorage.getItem('isLoggedIn').then((val) => {

      this.setState({isLoggedIn:val})
    })

  }

  backHandler = () => {
    if (this.props.isActionEnabled) {
      this.closeActionBtn()
      this.props.actionToggler()
      return true
    } else {
      return false
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.isActionEnabled == false) {
      this.closeActionBtn()
    }
  }

  closeActionBtn() {
    this.actionbutton.reset()
  }


  render() {
    return (
          <View>
            { this.state.isLoggedIn =='true' &&
          <ActionButton
            backdrop={() => (<View style={{ backgroundColor: '#000', position: 'absolute', width: global.deviceWidth }}></View>)}
            ref={_this => this.actionbutton = _this}
            degrees={0}
            onPress={() => this.props.actionToggler()}
            offsetX={20}
            renderIcon={(active) => (active ? <Icon name='md-close' size={20} /> : <Icon name='ios-more' size={20} />)} size={40} buttonColor="rgba(0,0,0,0)">
            <ActionButton.Item
              useNativeFeedback={true}
              buttonColor="#4A90F4"
              title="Profile"
           
              onPress={() => NavigationService.navigate('Profile', {})}
            >
              <FontAwesome name="user" style={styles.actionButtonIcon} />
            </ActionButton.Item>
            <ActionButton.Item
              buttonColor="#4A90F4"
              title="Settings"
              
              onPress={() => NavigationService.navigate('settings', {})}>

              <FontAwesome name="cog" style={styles.actionButtonIcon} />
            </ActionButton.Item>

            <ActionButton.Item
              buttonColor="#4A90F4"
              title="Saved"
             
              onPress={() => NavigationService.navigate('saved', {})}>
              <FontAwesome name="bookmark" style={styles.actionButtonIcon} />
            </ActionButton.Item>


            <ActionButton.Item
              buttonColor="#4A90F4"
              title="My Activity"
             
              onPress={() => NavigationService.navigate('activity', {})}>
              <FontAwesome name="bookmark" style={styles.actionButtonIcon} />
            </ActionButton.Item>

          </ActionButton>
         } 
      </View>
    );
  }
}


const styles = {
  actionButtonIcon: {
    fontSize: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    color: "white"

  },

  backgroundOverlay_actionBtn: {
    position: 'absolute',
    width: global.deviceWidth,
    height: global.deviceHeight,
    backgroundColor: 'rgba(150,150,150,0.6)'
  }
};

export default More;
