import React, { Component } from 'react'
import { Text, View, Image, StyleSheet,FlatList } from 'react-native'
import { ListItem } from "react-native-elements"
import UserDataHolder from "../../Login_SignUp/UserDataHolder"
import HelperMethods from "../../Helper/Methods"
import LottieView from 'lottie-react-native'
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner';
import FollowerItem from './FollowerItem';

const LottieLoader = LottieSpinner(View)

const placeHolderImage = require('../../../assets/images/userPlaceholder.png')





export default class Followers extends Component {

  state = {
    myFollowers: [],
    isLoadingFeeds: false,
    noFeeds: false,

  }
  static navigationOptions =
    {
      headerTintColor: global.primaryColor,
    };

  componentDidMount() {
    this.getFollowersList()
  }



  getFollowersList = () => {

    let { cat_name } = this.props.navigation.state.params
    this.setState({ isLoadingFeeds: true })
    const formData = new FormData();
    formData.append("user_id", UserDataHolder.userId);
    formData.append("profile_id", cat_name.id);
    formData.append("access_token", UserDataHolder.access_token);
    formData.append("flag_view", '0');
    formData.append("offset", '0');


    HelperMethods.makeNetworkCall_post("user_follow", formData, (response) => {
      this.setState({ isLoadingFeeds: false })
      if (response.flag == 1) {
        this.setState({ myFollowers: response.data, noFeeds: false })
      } else {
        this.setState({ noFeeds: true })
      }
    })
  }


  renderFeeds = ({ item, index }) => {
    return (
        <FollowerItem
            key={item.id}
            name = {item.first_name + " " + item.last_name}
            user_id = {item.id}
            isFollowing = {item.is_follow}
            image_url = {item.profile_image}
            currentNavigation = {this.props.navigation} 
        />
    )
}


  render() {
    return (
      <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>

        <View style={{ flex: 1, }}>
          {
            <FlatList
              style={{ marginBottom: 20, }}
              data={this.state.myFollowers}
              renderItem={this.renderFeeds}
            />
          }
          {
            this.state.noFeeds &&
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <LottieView
                style={{ height: 100, width: 100, }}
                source={require('../../../assets/lottieAnims/noResults.json')}
                autoPlay
                loop
              />
              <Text>No followers </Text>
            </View>
          }
        </View>
      </LottieLoader>

    )
  }
}
const styles = StyleSheet.create({

  photo: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
  },
})