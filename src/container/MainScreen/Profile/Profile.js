import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  Image, Alert,
  TouchableWithoutFeedback, Dimensions
} from 'react-native';
import { Button, Card } from "native-base";

import { Avatar } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import UserDataHolder from '../../Login_SignUp/UserDataHolder';
import HelperMethods from '../../Helper/Methods';
import FeedsListItem from "../Home/components/FeedsListItem";
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner';
import { ScrollView } from 'react-native-gesture-handler';



const options = {
  title: 'Set a Profile Picture',
  takePhotoButtonTitle: 'Camera',
  chooseFromLibraryButtonTitle: 'Gallery',
  remove: 'Remove'
}

const LottieLoader = LottieSpinner(View)

export default class Profilepage extends Component {

  state = {
    myfeeds: [],
    clickedMenu: false,
      avatarSource: UserDataHolder.profile_image,
      follower: UserDataHolder.follower,
      following: UserDataHolder.following,
      posts: UserDataHolder.numberOfPosts,
      name: UserDataHolder.name,
      about_me: UserDataHolder.about_me,

  }

  static navigationOptions =
    {
      headerTintColor: global.primaryColor,
    };

  componentWillMount() {
    this.props.navigation.addListener('willFocus',this.refreshData)
    this.getActivityData()
    this.props.navigation.navigate('editprofile')

  }


  refreshData = () => {
    this.setState ({about_me : UserDataHolder.about_me})
    this.setState ({name : UserDataHolder.name})
    this.setState ({avatarSource : UserDataHolder.profile_image})
}

  onShare = async () => {
    try {
        const result = await Share.share({
            message:
                'React Native | A framework for building native apps using React',
        });

        if (result.action === Share.sharedAction) {
            if (result.activityType) {
                // shared with activity type of result.activityType
            } else {
                // shared
            }
        } else if (result.action === Share.dismissedAction) {
            // dismissed
        }
    } catch (error) {
        alert(error.message);
    }
};

  goToFollow() {
      HelperMethods.navigateToScreen(this.props.navigation,{'id' : UserDataHolder.userId},'followers')
  }

  goToFollowing() {
    HelperMethods.navigateToScreen(this.props.navigation,{'id' : UserDataHolder.userId},'following')
  }


  goToMyPosts() {
    HelperMethods.navigateToScreen(this.props.navigation,{'id' :  UserDataHolder.userId},'myposts')    
  }

  showAlertBox(feedId) {
    let index = this.state.myfeeds.findIndex(v => v.id == feedId)
    this.setState({ clickedMenu: true })
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   avatarSource: UserDataHolder.profile_image,
    //   follower: UserDataHolder.follower,
    //   following: UserDataHolder.following,
    //   posts: UserDataHolder.numberOfPosts,
    //   name: UserDataHolder.name,
    //   about_me: UserDataHolder.about_me,
    // }
  }


  mypic = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      let uri = response.uri
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        this.setState({
          avatarSource: uri,
          pic: response.data
        });
      }
    });
  }

  getActivityData = () => {

    this.setState({ isLoadingFeeds: true })
    const formData = new FormData();
    formData.append("user_id", UserDataHolder.userId);
    formData.append("profile_id", UserDataHolder.userId);
    formData.append("access_token", UserDataHolder.access_token);
    HelperMethods.makeNetworkCall_post("user_activity", formData, (response) => {
      this.setState({ isLoadingFeeds: false })
      if (response.flag == 1) {
        this.setState({ myfeeds: response.data, noFeeds: false })
      } else {
        this.setState({ noFeeds: true })
      }
    })
  }

  renderFeeds = ({ item, index }) => {
    return (
      <FeedsListItem
        key={item.id}
        id={item.id}
        user_id = {item.user_id}
        menuActive={this.state.clickedFeedId == item.id}
        showFeedsUIHandler={(id) => this.showAlertBox(id)}
        title={HelperMethods.Capitalize(item.user_name)}
        subtitle={HelperMethods.Capitalize(item.created_date)}
        description={item.description}
        image_url={item.user_image}
        categories={item.categories_array}
        post_image_array={item.images}
        tags_array={item.tags_array}
        currentNavigation = {this.props.navigation} 

      />
    )
  }


  render() {

    const { navigate } = this.props.navigation;

    return (

      <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>
      <ScrollView>

        <View>

          <View style={{ marginTop: 10 }}>
            <Avatar
              rounded
              containerStyle={{ alignSelf: 'center' }}
              size={90}
              source={{ uri: this.state.avatarSource }}
          //    showImagePicker
          //    onPress={this.mypic}
           //   showEditButton
            />
          </View>


          <View style={styles.bodyContent}>

            <View style={{ marginTop: 10 }}>
              <Text style={styles.nameText}>{this.state.name}</Text>
            </View>

            <Text style={styles.description}>{this.state.about_me}</Text>

            <Button
              rounded
              transparent
              onPress={() => this.props.navigation.navigate("editprofile")}
              style={styles.btn}>
              <Text style={styles.btnText}>Edit Profile</Text>
            </Button>

          </View>

          <View style={styles.profileDetail}>

            <View style={styles.detailContent}>
              <Button onPress={() => this.goToFollow()} style={{ backgroundColor: '#fff' }}>

                <Text style={styles.count}>{this.state.follower}</Text>
                <Text style={styles.title}> Followers</Text>
              </Button>

            </View>

            <View style={styles.line_vertical} />

            <View style={styles.detailContent}>
              <Button onPress={() => this.goToFollowing()} style={{ backgroundColor: '#fff' }}>
                <Text style={styles.count}>{this.state.following}</Text>
                <Text style={styles.title}> Following</Text>
              </Button>

            </View>

            <View style={styles.line_vertical} />

            <View style={styles.detailContent}>
              <Button onPress={() => this.goToMyPosts()} style={{ backgroundColor: '#fff' }}>

                <Text style={styles.count}>{this.state.posts}</Text>
                <Text style={styles.title}> Posts</Text>
              </Button>

            </View>

          </View>

          <View style={styles.line} />

          <View style={styles.recentHeader}>
            <Text style={styles.headerTitle}>Recent Activities</Text>
          </View>
          <FlatList
            style={{ marginBottom: 380 }}
            data={this.state.myfeeds}
            renderItem={this.renderFeeds}
          />
        </View>
        {
          this.state.clickedMenu &&
          <View style={{
            position: 'absolute', flex: 1,
            zIndex: 100, width: Dimensions.get('window').width, height: Dimensions.get('window').height, alignItems: 'center', justifyContent: 'center'
          }}>
            <TouchableWithoutFeedback onPress={() => this.setState({ clickedMenu: false })}>
              <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', width: '100%', height: '100%' }}>

              </View>
            </TouchableWithoutFeedback>
            <Card style={{ width: Dimensions.get('window').width, height: 200, position: 'absolute', bottom: Dimensions.get('window').height / 2 }}>
              <Button onPress={this.onShare}
                transparent
              >
                <Text style={{ color: 'black' }}>   Share</Text>
              </Button>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("")}>
                <Text style={{ color: 'black' }}>   Follow this post</Text>
              </Button>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("")}>
                <Text style={{ color: 'black' }}>   Hide this post</Text>
              </Button>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("")}>
                <Text style={{ color: 'black' }}>   Report</Text>
              </Button>
            </Card>
          </View>

        }
</ScrollView>
      </LottieLoader>
    );
  }
}

const styles = {

  nameText: {
    fontSize: 16,
    color: "#000",
    // fontFamily: global.fontFamily_Medium
  },
  description: {
    fontSize: 14,
    color: "#000",
    marginTop: 10,
    textAlign: 'center',
    // fontFamily: global.fontFamily_Regular
  },
  btn: {
    borderColor: "#4A90F4",
    alignSelf: 'center',
  },
  btnText: {
    fontSize: 11,
    color: '#4A90F4',
    //  fontFamily: global.fontFamily_Regular
  },
  bodyContent: {
    alignItems: 'center',
    marginRight: 30,
    marginLeft: 30
  },

  recentHeader: {
    marginTop: 20,
  },

  headerTitle: {
    fontSize: 18,
    color: '#000',
    marginLeft: 20,
    marginTop: 10,
    fontWeight: "500",
  },
  line_vertical: {
    width: 1,
    height: 40,
    backgroundColor: global.primaryColor,
  },
  profileDetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 50,
    marginLeft: 50,
  },
  detailContent: {
    alignItems: 'center'
  },
  count: {
    fontSize: 20,
    color: "#4A90F4",
    //  fontFamily: global.fontFamily_Bold,
  },
  title: {
    fontSize: 14,
    color: '#000',
    //fontFamily: global.fontFamily_Regular
  },
  line: {
    backgroundColor: "#D5D5D5",
    height: 1,
    width: "100%",
    marginTop: 25
  },

}
