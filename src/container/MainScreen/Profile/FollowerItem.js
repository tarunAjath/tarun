import React from 'react';
import { View, StyleSheet, Image, TouchableWithoutFeedback } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import HelperMethods from '../../Helper/Methods'
import { Container, Header, Content, Button, Icon, Text } from 'native-base';
import UserDataHolder from '../../Login_SignUp/UserDataHolder'
const placeHolderImage = require('../../../assets/images/userPlaceholder.png')


const setUserImage = (userImageURL) => {

    if (userImageURL) {
        return <Image source={{ uri: userImageURL }} style={styles.photo} />
    } else {
        return <Image source={placeHolderImage} style={styles.photo} />

    }
}

const followUser = (followType,profile_id) => {

        const formData = new FormData();
        formData.append("user_id", UserDataHolder.userId);
        formData.append("profile_id", profile_id);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append("flag_view", '1');
        formData.append("offset", '0');
        formData.append("follow_type", followType);
    
        HelperMethods.makeNetworkCall_post("user_follow", formData, (response) => {
          if (response.flag == 1) {
            alert("User Follwed")
          } 
        })
      }
    


const FollowerItem = ({ name, user_id, isFollowing, image_url, currentNavigation }) => (
    <View>

        <View style={styles.topContainer}>
            <View>
                {setUserImage(image_url)}
            </View>
            <View style={{ marginLeft: 16, marginRight: 20, flex: 1, flexDirection: "row" }}>
                <Button transparent style={{ alignItems: "flex-start" }}
                    onPress={() => goToOtherUserProfiles(user_id, currentNavigation)}
                >
                    <Text style={{ color: global.primaryColor }}>{name}</Text>
                </Button >
                {
                    isFollowing == '0' ?
                  
                        <TouchableWithoutFeedback onPress={() => followUser("1",user_id)}>
                        <View
                            style={{
                                borderWidth: 1, borderRadius: 20, borderColor: '#4A90F4', alignItems: 'center', justifyContent: 'center',
                                margin: 5

                            }}>

                            <Text style={{ color: '#4A90F4', paddingLeft: 5, fontSize: 13, paddingRight: 5, paddingTop: 5, paddingBottom: 5 }}>Follow</Text>
                        </View>
                    </TouchableWithoutFeedback>
                        
                        :
                        <TouchableWithoutFeedback onPress={() => followUser("0",user_id)}>
                        <View    
                            style={{
                                borderWidth: 1, borderRadius: 20,borderColor: "#4A90F4", backgroundColor: '#4A90F4', alignItems: 'center', justifyContent: 'center',
                                margin: 2
                            }}>

                            <Text style={{ color: '#fff', paddingLeft: 5, fontSize: 13, paddingRight: 5, paddingTop: 5, paddingBottom: 5 }}>Following</Text>
                        </View>
                    </TouchableWithoutFeedback>
                }


            </View>
        </View>
    </View>
);


const goToOtherUserProfiles = (id, currentNavigation) => {
    HelperMethods.navigateToScreen(currentNavigation, { 'id': id }, 'otherUserProfile')
}




const styles = StyleSheet.create({
    photo: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
    },
    topContainer: {
        flexDirection: 'row',
        padding: 16,
        backgroundColor: '#FFF',
        elevation: 2,
    },
})
export default FollowerItem;
