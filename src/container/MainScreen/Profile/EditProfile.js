import React, { Component } from "react";
import { View, Text, ScrollView, TouchableOpacity, TouchableWithoutFeedback, Dimensions, Keyboard } from 'react-native';
import { Input, Item, Label, Container, Header, Content, DatePicker } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import { Avatar } from 'react-native-elements';
import { Button } from 'native-base';
import "../../Helper/Global";
import UserDataHolder from '../../Login_SignUp/UserDataHolder'
import MaterialIcon from "react-native-vector-icons/MaterialIcons"
import ImagePicker from 'react-native-image-picker';
import { RNS3 } from 'react-native-aws3';
import { isFlowBaseAnnotation } from "@babel/types";
import UpdateDataHolder from "./components/UpdateDataHolder";
import HelperMethods from ".././../Helper/Methods"
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner'

const options = {
  title: 'Set a Profile Picture',
  takePhotoButtonTitle: 'Camera',
  chooseFromLibraryButtonTitle: 'Gallery',
  aspect: [4, 3]
}

let _this = null;

const LottieLoader = LottieSpinner(View)

export default class EditProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chosenDate: new Date(),
      showDatePicker: false
    };
    this.setDate = this.setDate.bind(this);
  }

  state = {
    username: UserDataHolder.name,
    avatarSource: UserDataHolder.profile_image,
    pic: null,
    date: "2016-05-15",
    showDatePicker: false,
    about_me: UserDataHolder.about_me,
    selectedCats: [],
    mediaUri: UserDataHolder.profile_image,
    gender: UserDataHolder.gender,
    isUploadingImgae: false,
  }


  static navigationOptions = {

    headerRight: (
      <Button transparent
        onPress={() => _this.saveUserProfile()}
      >
        <Text style={{ color: global.primaryColor }}>Submit    </Text>
      </Button>
    ),
  };

  componentWillMount() {
    _this = this;
    this.props.navigation.setParams({ handleSave: () => this.saveUserProfile() })
    this.dPicker = null
    this.setState({ username: UserDataHolder.name, });
    this.setState({ about_me: UserDataHolder.about_me, });
    this.setState({ avatarSource: UserDataHolder.profile_image });
    UpdateDataHolder.cats = UserDataHolder.subscribe_category.split(',')
    this.props.navigation.addListener('willFocus', this.willFocus)

    //alert(this.state.gender)
  }



  saveUserProfile = () => {

    if (this.state.username.length == 0) {
      alert('Please enter your registered email')
    }

    else {

      Keyboard.dismiss()

      alert(this.state.mediaUri)
      const formData = new FormData()
      formData.append("name", this.state.username)
      formData.append("access_token", UserDataHolder.access_token)
      formData.append("user_id", UserDataHolder.userId)
      formData.append("flag_view", 0)
      formData.append("timezone", "IST")
      formData.append("profile_image", this.state.mediaUri)
      formData.append("about", this.state.about_me)
      formData.append("gender", this.state.about_me)

      HelperMethods.makeNetworkCall_post("save_user_profile", formData, response => {
        if (response.flag == 1) {
       alert("Profile Updated Successfully")

          UserDataHolder.name = this.state.username
          UserDataHolder.about_me = this.state.about_me
          HelperMethods.updateUserData(UserDataHolder.name , UserDataHolder.email , UserDataHolder.about_me,UserDataHolder.profile_image)
        } else {
          //  alert(JSON.stringify(response.msg))
        }
      })
    }
  }

  uploadImageToS3 = () => {

    const file = {
      uri: this.state.avatarSource,
      name: 'photo.jpg',
      type: 'jpg'
    };
    const options = {
      keyPrefix: 'photos/',
      bucket: 'nimmio-s3',
      region: 'ap-south-1',
      accessKey: 'AKIA2EDSUISQWORMPSHF',
      secretKey: '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',
    };
    RNS3.put(file, options).then(response => {
      if (response.status !== 201) {
        alert("Error Uploading image : ", JSON.stringify(error))
        throw new Error('Failed to upload image to S3', response);
      }
        this.setState({ mediaUri: response.body.postResponse.location })
       this.setState({ isUploadingImgae: false })
      this.updateImage()
    });

  }


  updateImage() {
    Keyboard.dismiss()
    const formData = new FormData()
    formData.append("access_token", UserDataHolder.access_token)
    formData.append("user_id", UserDataHolder.userId)
    formData.append("profile_image", this.state.mediaUri)
    formData.append("flag_view", 1)
    formData.append("timezone", "IST")

    HelperMethods.makeNetworkCall_post("save_user_profile", formData, response => {
      if (response.flag == 1) {
        UserDataHolder.profile_image = this.state.mediaUri
        this.setState({ isUploadingImgae: false })
        HelperMethods.updateUserData(UserDataHolder.name , UserDataHolder.email , UserDataHolder.about_me,UserDataHolder.profile_image,UserDataHolder.date_of_birth)
        alert(response.msg)
      } else {
          alert(JSON.stringify(response.msg))
          this.setState({ isUploadingImgae: false })
      }
    })
  }



willFocus = () => {
  if (UpdateDataHolder.updateCat) {
    this.setState({})
    UpdateDataHolder.updateCat = false
  } else {
    UpdateDataHolder.cats = UpdateDataHolder.cats
  }
}

updateCats() {

}



setDate(newDate) {
  let date = this.formatChosenDate(newDate)

  this.setState({ chosenDate: date });
}

formatChosenDate(date) {
  if (this.props.formatChosenDate) {
    return this.props.formatChosenDate(date);
  }
  return [
    date.getDate(),
    date.getMonth() + 1,
    date.getFullYear(),
  ].join('/');
}




renderDatePicker = () => {
  return (

    <DatePicker
      ref={dPicker => this.dPicker = dPicker}
      defaultDate={new Date(2018, 4, 4)}
      minimumDate={new Date(2018, 1, 1)}
      maximumDate={new Date(2018, 12, 31)}
      locale={"en"}
      timeZoneOffsetInMinutes={undefined}
      modalTransparent={false}
      animationType={"fade"}
      androidMode={"default"}
      textStyle={{ color: "#000" }}
      placeHolderTextStyle={{ color: "#d3d3d3" }}
      onDateChange={this.setDate}
      modalVisible={true}
      disabled={false}
    />


  );
}

showDatePicker() {
  alert("showing")
  this.setState({ showDatePicker: true })
}

mypic = () => {

  ImagePicker.showImagePicker(options, (response) => {
    console.log('Response = ', response);

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    }
    else {
      this.setState({
        avatarSource: response.uri,
        pic: response.data
      });
      this.uploadImageToS3()
      this.setState({ isUploadingImgae: true })
    }
  });
}

render() {
  let nationalityData = [{
    value: 'British',
  }, {
    value: 'American',
  }, {
    value: 'Indian',
  }];

  let gender = [{
    value: 'Female',
  }, {
    value: 'Male',
  }
  ];

  return (

    <LottieLoader isApiCall={this.state.isUploadingImgae} style={{ flex: 1, }}>

    <View style={{ marginRight: 15, marginLeft: 15 }}>

      <ScrollView>
        <View style={{ marginTop: 10 }}>
          <Avatar
            rounded
            containerStyle={{ alignSelf: 'center' }}
            size={90}
            source={{ uri: this.state.avatarSource }}
            showImagePicker
            onPress={this.mypic}
            showEditButton
          />
        </View>

        <View style={{ alignSelf: 'center', marginTop: 10 }}>
          <Text style={styles.nameText}>{UserDataHolder.name}</Text>
        </View>

        <View style={{ marginTop: 15 }}>
          <Item floatingLabel>
            <Label>Name/Username</Label>
            <Input
              placeholderTextColor="#000"
              value={this.state.username}
              onChangeText={text => this.setState({ username: text })}
            />
          </Item>
        </View>

        <View style={{ marginTop: 15 }}>
          <Item floatingLabel>
            <Label>Bio</Label>
            <Input
              multiline={false}
              maxLength={100}
              placeholderTextColor="#000"
              value={this.state.about_me}
              onChangeText={text => this.setState({ about_me: text })} />
          </Item>
        </View>
        <View>
          <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'space-between' }}>
            <Text style={styles.title}>Interest Categories</Text>
            <TouchableWithoutFeedback
              onPress={() => this.props.navigation.navigate('Categories_More', { 'selectedCats_user': UserDataHolder.subscribe_category, 'role': 'editProfile' })}>
              <MaterialIcon name="edit" size={16} />
            </TouchableWithoutFeedback>
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', }}>
            {UpdateDataHolder.cats.map((item) => {
              return (
                <Text>{item}, </Text>
              )
            })}
          </View>

        </View>

        <View style={{ marginTop: 10 }}>
          {/* <CategoryButton/> */}
        </View>

        <View style={{ marginTop: 15 }}>
          <Text style={[styles.title, { marginBottom: -30 }]}>Nationality</Text>
          <Dropdown

            data={nationalityData} />
        </View>

        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 15 }}>
          <View style={{ flex: 1, paddingRight: 10, marginTop: 10 }}>
            <Text style={styles.title}>DOB</Text>
            {
              this.renderDatePicker()
            }
            <View style={{ backgroundColor: "#eee", width: global.deviceWidth / 2 - 20, height: 1 }}></View>
          </View>

          <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 10, }}>
            <Text style={styles.title}>Gender</Text>
            <Dropdown
              fontSize={14}
              data={gender}
              value = {this.state.gender}
            />
          </View>
        </View>

      </ScrollView>
    </View>
    </LottieLoader>
  );
}
}

const styles = {
  nameText: {
    fontSize: 16,
    color: "#000",
    fontFamily: global.fontFamily_Medium
  },
  title: {
    color: '#000',
    fontFamily: global.fontFamily_Regular,
    fontSize: 16,

  },
}