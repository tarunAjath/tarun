import React, { Component } from "react";
import { Text, View, ImageBackground, Button, Dimensions, TouchableHighlight } from "react-native";
import {NavigationActions,StackActions} from 'react-navigation'
import HelperMethods from "../../Helper/Methods";
import AntDesign from 'react-native-vector-icons/AntDesign'
import DataHolder from "../AddPost/Helpers/DataHolder";
import UserDataHolder from "../../Login_SignUp/UserDataHolder";
import UpdateDataHolder from "../Profile/components/UpdateDataHolder";
import FilterDataHolder from "../Home/Helpers/FilterDataHolder";

let editCats = ''
export default class CustomCategories extends Component {

  state = {
    isSelected:false
  }

  componentWillMount(){
    let role = this.props.role
    if(role == 'editProfile'){
      if(UpdateDataHolder.cats.findIndex(v => v == this.props.category_id ) > -1  ){
        this.setState({isSelected:true})
      }
    } else if (role == 'addPost') {
      this.setSelected(DataHolder.cat)
    } else if (role == 'search' ){
      this.setSelected(FilterDataHolder.cats)
    }
   }

   setSelected(data){
    if(data.findIndex(v => v.category_id == this.props.category_id ) > -1  ){
      this.setState({isSelected:true})
    }
   }

  _choosen = (category_id, category_name,currentnavigation,role) => {
    
    if(role == 'addPost'){ 
      if(DataHolder.cat.length < 3){
        this.setState({isSelected:!this.state.isSelected})
        this.state.isSelected ? this.props.removeCat(category_id) : this.props.addCat(category_id,category_name)
      } else if(this.state.isSelected) {
        this.setState({isSelected:false})
        this.props.removeCat(category_id)
      } else {
        alert('Only 3 can be selected')
      }
      return
    } else if(role == 'editProfile'){
      
      if(this.state.isSelected){
        UpdateDataHolder.cats.splice(UpdateDataHolder.cats.findIndex(v => v == category_id),1)
      } else {
        UpdateDataHolder.cats.push(category_id)
      }

      this.setState({isSelected:!this.state.isSelected})
      return
      // UserDataHolder.subscribe_category = editCats
    } else if(role == 'search'){
      if(this.state.isSelected){
        FilterDataHolder.cats.splice(FilterDataHolder.cats.findIndex(v => v == category_id),1)
      } else {
        FilterDataHolder.cats.push({category_name,category_id})
      }

      this.setState({isSelected:!this.state.isSelected})
      return
    }

    HelperMethods.navigateToScreen(currentnavigation,{'title' : category_name,'id' : category_id },'CategoryFeeds')
  }

  render(){
    return(
        <TouchableHighlight
          onPress={() => this._choosen(this.props.category_id, this.props.category_name,this.props.navigation,this.props.role)}
          underlayColor={"#ffffff"}>

          <View style={styles.container}>
            <ImageBackground source={{ uri: this.props.category_url }} style={styles.categoryImage}>
            {
              this.state.isSelected ?
              <View style={styles.circleView}>
                <AntDesign name='check' color='#fff' size={25} />
              </View>
              :
              null
            }
                
            </ImageBackground>
            <View style={styles.textcontainer}>
                <Text style={styles.categoryName}>{this.props.category_name}</Text>
              </View>

          </View>
        </TouchableHighlight>
    );
  }
}

const styles = {
  container: {
    paddingRight: 10,
    paddingLeft: 10,
    marginBottom: 30,
    justifyContent: "center",
    alignItems: "center",
    height : 110
  },

  circleView: {
  width: 30,
  height: 30,
  borderRadius: 100/2,
  backgroundColor: global.primaryColor,
  justifyContent: "center",
    alignSelf: "center"
  },


  textcontainer: {
    marginBottom: 20,
    backgroundColor: "#fff",
    marginTop: 2,
    justifyContent: "center",
    alignItems:'center',
  },
  categoryName: {
    fontSize: 12,
    color: '#000',
  },

  categoryImage: {
    height: 100,
    width: 110,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    paddingTop: 5,
    alignItems:'center',
    justifyContent:'center'
  
  },

};
