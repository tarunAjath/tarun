
import React, { Component } from 'react'
import {
    View,
    Dimensions,
    FlatList,
    TouchableWithoutFeedback,
    Text, Share,
    Alert

} from "react-native";
import UserDataHolder from '../../../container/Login_SignUp/UserDataHolder'
import HelperMethods from '../../../container/Helper/Methods';
import CustomRow from '../Home/components/FeedsListItem'
import { Button, Card } from "native-base";
import LottieView from 'lottie-react-native';
import FeedsListItem from '../Home/components/FeedsListItem'
import LottieSpinner from '../../../Helpers/HOC/LottieSpinner';

const LottieLoader = LottieSpinner(View)


export default class CategoryFeeds extends Component {

    state = {
        feeds: [],
        isLoadingFeeds: false,
    }

    


    static navigationOptions = ({ navigation }) => ({
        title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? '' : navigation.state.params.title,
        headerTintColor: global.primaryColor,

    });


    componentWillMount() {
        this.setState({ isLoadingFeeds: true })
        let { cat_name } = this.props.navigation.state.params
        this.props.navigation.setParams({ title: cat_name.title })
        this.findCoordinates()
    }


    findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                const location = JSON.stringify(position);
                UserDataHolder.currentLatitude = position.coords.latitude,
                    UserDataHolder.currentLongitude = position.coords.longitude,
                    this.setState({ location });
                this.getCategoriesData()
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }

        );

    };


    getCategoriesData = () => {
        const formData = new FormData();
        let { cat_name } = this.props.navigation.state.params
        formData.append("user_id", UserDataHolder.userId);
        formData.append("access_token", UserDataHolder.access_token);
        formData.append("lat", UserDataHolder.currentLatitude);
        formData.append("long", UserDataHolder.currentLongitude);
        formData.append("categories", cat_name.id);
        formData.append("flag_view", '0');
        formData.append("offset", '');
        formData.append("lang", '');
        formData.append("timezone", 'IST');

        HelperMethods.makeNetworkCall_post("feeds_list", formData, (response) => {
            this.setState({ isLoadingFeeds: false })

            if (response.flag == 1) {
                this.setState({ feeds: response.data })

            } else if (response.Status == 0) {
                alert("Wrong email or password")
            }
            else {
                alert("No response from the API")
            }
        })
    }


    onShare = async () => {
        try {
            const result = await Share.share({
                message:
                    'React Native | A framework for building native apps using React',
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    updateBookmarkIcon(id,currentStatus){
        let index = this.state.feeds.findIndex(v => v.id == id )
        if(index >  -1){
            let arr = this.state.feeds
            arr[index].is_saved = currentStatus == '1' ? '0' : '1'
            this.setState({feeds:arr})
        }
        
    }

    updateLikeIcons(id,currentStatus){
        let index = this.state.feeds.findIndex(v => v.id == id )
        if(index >  -1){
            let arr = this.state.feeds
            arr[index].is_like = currentStatus == '1' ? '0' : '1'
            this.setState({feeds:arr})
        }
        
    }


    renderFeeds = ({ item, index }) => {
        return (
            <FeedsListItem
                key={item.id}
                id={item.id}
                user_id = {item.user_id}
                menuActive={this.state.clickedFeedId == item.id}
                showFeedsUIHandler={(id) => this.showAlertBox(id)}
                title={HelperMethods.Capitalize(item.user_name)}
                subtitle={HelperMethods.Capitalize(item.created_date)}
                description={item.description}
                updateBookmarkIcons={(id,currentStatus) => this.updateBookmarkIcon(id,currentStatus)}
                updateLikeIcons={(id,currentStatus) => this.updateLikeIcons(id,currentStatus)}
                savedStatus={item.is_saved}
                likeStatus={item.is_like}
                image_url={item.user_image}
                categories={item.categories_array}
                post_image_array={item.images}
                tags_array={item.tags_array}
                total_like  = {item.total_like}
                total_comment  = {item.total_comment}
                currentNavigation = {this.props.navigation} 
            />
        )
    }

    
    showAlertBox(feedId) {
        let index = this.state.feeds.findIndex(v => v.id == feedId)
        this.setState({ clickedFeed: true })
    }

    render() {

        return (
            <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>

            <View style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height - 100 }}>
                <FlatList
                    style={{ flex: 1, marginBottom : 35 }}
                    data={this.state.feeds}
                    renderItem={this.renderFeeds}
                    contentInset={{ x: 0, y: this.state.feeds.length * 300 }}
                />
                {
                    this.state.clickedFeed &&
                    <View style={{
                        position: 'absolute', flex: 1,
                        zIndex: 100, width: Dimensions.get('window').width, height: Dimensions.get('window').height, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <TouchableWithoutFeedback onPress={() => this.setState({ clickedFeed: false })}>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.8)', width: '100%', height: '100%' }}>

                            </View>
                        </TouchableWithoutFeedback>
                        <Card style={{ width: Dimensions.get('window').width, height: 200, position: 'absolute', bottom: Dimensions.get('window').height / 2 }}>
                            <Button onPress={this.onShare}
                                transparent
                            >
                                <Text style={{ color: 'black' }}>   Share</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Follow this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Hide this post</Text>
                            </Button>
                            <Button
                                transparent
                                onPress={() => this.props.navigation.navigate("")}>
                                <Text style={{ color: 'black' }}>   Report</Text>
                            </Button>
                        </Card>
                    </View>

                }
            </View>
            </LottieLoader>

        );


    }
}
const styles = {
    circleView: {
        width: 50,
        height: 50,
        borderRadius: 100,
        borderColor: "#fff",
        backgroundColor: '#42AACC',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        marginLeft: 170,
        marginBottom: 150,
    },

    plusIcon: {
        borderColor: "#fff",
        borderRadius: 4,
        justifyContent: 'center'
    }
    ,
    overlay:  {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor : 'rgba(52, 52, 52, 0.1)',
       }
}



