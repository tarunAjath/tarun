import React, { Component, StyleSheet } from "react";
import { Text, View } from "react-native";
import {Button} from 'native-base'
import CustomCategories from "./CustomCategory";
import UserDataHolder from "../../Login_SignUp/UserDataHolder";
import GridList from "react-native-grid-list";
import HelperMethods from '../../../container/Helper/Methods';
import DataHolder from "../AddPost/Helpers/DataHolder";
import LottieSpinner from "../../../Helpers/HOC/LottieSpinner";
import UpdateDataHolder from '../Profile/components/UpdateDataHolder'

let catRole = ''
let selectedCats = []
let userCats = ''
let navigation = null
const LottieView = LottieSpinner(View)

const setCategories = () => {
  switch(catRole){
    case 'editProfile':
    UpdateDataHolder.updateCat = true
    navigation.pop()
    break

    case 'addPost':
    navigation.pop()
    break

    default:
    navigation.pop()
    break
  }
}

export class Categories extends Component {
  state = {
    categories: [],
    isLoadingCategories: false,
  }

  static navigationOptions =  ({navigation}) => ({
    
    headerRight: (
      <View>
        
        {/* {alert(navigation.state.params)} */}
        { (navigation.state.params != undefined && navigation.state.params.role == 'selection' )  &&
        <Button transparent
        onPress={() => setCategories()}
        >
      <Text style = {{color :global.primaryColor,fontWeight:'bold'}}>Done   </Text>
      </Button>
      }
        </View>
    ),
  })

  setCategories(){
    switch(catRole){
      case 'editProfile':
      this.props.navigation.pop()
      break
  
      case 'addPost':
      break
    }
  }

  componentWillMount(){
    navigation = this.props.navigation
    if(this.props.navigation.state.params != null){

      const {role,selectedCats_user} = this.props.navigation.state.params
      userCats = selectedCats_user
      catRole = role
    }
  }
  componentDidMount() {
    let { routeName} = this.props.navigation.state

    if(routeName == 'Categories_More' || routeName == 'categories_search' || routeName == 'Categories_addPost'  )
    this.props.navigation.setParams({role:'selection'})
    
    this.setState({ isLoadingCategories: true })
    this.getCategoryData()
  }

  getCategoryData = () => {

    const formData = new FormData();
    formData.append("user_id", UserDataHolder.userId);
    formData.append("access_token", UserDataHolder.access_token);
    formData.append("timezone", "IST");

    HelperMethods.makeNetworkCall_post("categories", formData, response => {
      if (response.flag == 1) {
        console.warn(response.data)
        this.setState({ isLoadingCategories: false })
        this.setState({ categories: response.data });
      } else if (response.Status == 0) {
        alert("Wrong email or password");
      } else {
        alert("No response from the API");
      }
    });
  };

  addCat(category_id,category_name){
    if(DataHolder.cat.length < 3){
      DataHolder.cat.push({category_id,category_name})
    } else {
      alert('Only 3 can be selected')
    }
  }

  removeCat(id){
    let index = DataHolder.cat.findIndex(v => v.category_id == id )
    DataHolder.cat.splice(index,1)
  }

  

  renderCategories = ({ item, index }) => {
    return (
      <CustomCategories
        
        role={catRole}
        category_url={item.category_image}
        category_name={item.category_name}
        category_id={item.id}
        userCats={ userCats != undefined &&  userCats.length > 0 ? userCats : null }
        navigation={this.props.navigation}
        addCat={(id,name)=> this.addCat(id,name)}
        removeCat = { (id) => this.removeCat(id) }
      />
    );
  };

  render() {
    return (

      <LottieView isApiCall={this.state.isLoadingCategories} style={{flex:1 }}>
                <View style={{marginTop:20, paddingBottom : 20}}>
                  <GridList
                    data={this.state.categories}
                    numColumns={3}
                    renderItem={this.renderCategories}
                    />
                </View>
      </LottieView>
    );
  }
}

const styles = {
  overlay:  {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor : 'rgba(150,150,150, 0.8)',
      justifyContent:'center'
     }
}

export default Categories;
