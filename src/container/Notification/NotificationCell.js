import React from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';


const placeHolderImage = require('../../assets/images/userPlaceholder.png')


setUserImage = (userImageURL) => {

    if (userImageURL) {
        return <Image source={{ uri: userImageURL }} style={styles.photo} />
    } else {
        return <Image source={placeHolderImage} style={styles.photo} />

    }
}

const NotificationCell = ({ id, name, message, image_url, timeStamp }) => (

    <View style={styles.container}>
        <View style={{ flexDirection: "row" }}>
            <View>
                {setUserImage(image_url)}
            </View>
            <Text style={styles.baseText}>
                <Text style={styles.titleText}>
                        {name} 
                </Text>
                <Text numberOfLines={5}> {message}</Text>
            </Text>
        </View>
        <View style={{ width: Dimensions.get('window').width - 20, height: 0.5, backgroundColor: "#707070", paddingBottom: 1 }} />
    </View>
);

const styles = StyleSheet.create({
    container: {
        marginLeft: 10,
        height: 90,
        paddingTop: 10,
        justifyContent: "space-between",
    },
    photo: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        paddingLeft: 20,
        marginLeft: 10,
    },
    paragraph: {
        margin: 0,
        fontSize: 15,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },

    baseText: {
        fontFamily: global.fontFamily,
        marginRight : 60,
        marginLeft : 20,
        fontSize: 14,
      },
      titleText: {
        fontSize: 14,
        fontWeight: 'bold',
        marginRight : 3
      },

});

export default NotificationCell;

