import React, { Component } from 'react'
import { View, Dimensions, FlatList } from 'react-native'
import HelperMethods from '../Helper/Methods'
import UserDataHolder from '../Login_SignUp/UserDataHolder'
import NotificatioCell from '../Notification/NotificationCell'
import LottieView from 'lottie-react-native'
import LottieSpinner from '../../Helpers/HOC/LottieSpinner'


const LottieLoader = LottieSpinner(View)


export default class NotificationList extends Component {


  state = {
    notifications: [],
    isLoadingNotification: false,
    isLoadingFeeds: false,
    noFeeds: false,
  }

  static navigationOptions =
    {
      headerTintColor: global.primaryColor,
      title: "Notifications"
    };


  componentWillMount() {
    this.setState({ isLoadingFeeds: true })
    this.getNotificationList()
  }

  getNotificationList = () => {
    const formData = new FormData();

    formData.append("user_id", UserDataHolder.userId);
    formData.append("access_token", UserDataHolder.access_token);
    formData.append("timezone", 'IST');

    HelperMethods.makeNetworkCall_post("notifications", formData, (response) => {
      this.setState({ isLoadingFeeds: false })
      if (response.flag == 1) {
        this.setState({ notifications: response.data })
      } 
      else {
        this.setState({ noFeeds: true })
        alert("No response from the API")
      }
    })
  }

  //  id, name,mssage,image_url, timeStamp }) => (


  renderNotifications = ({ item, index }) => {
    return (
      <NotificatioCell
        id={item.id}
        name={item.username}
        message={item.message}
        timeStamp={item.time}
        image_url={item.user_images}
      />
    )
  }

  render() {
    return (
   
      <LottieLoader isApiCall={this.state.isLoadingFeeds} style={{ flex: 1, }}>

        <View style={{ flex: 1, }}>
          {
           <View style={{ width: Dimensions.get('window').width, backgroundColor: "#fff", height: Dimensions.get('window').height - 100 }}>

           <FlatList
             extraData={this.state}
             style={{ marginBottom: 60 }}
             data={this.state.notifications}
             renderItem={this.renderNotifications}
           />
 
 
         </View>

          }
          {
            this.state.noFeeds &&
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <LottieView
                style={{ height: 100, width: 100, }}
                source={require('../../assets/lottieAnims/noResults.json')}
                autoPlay
                loop
              />
              <Text>No notifications</Text>
            </View>
          }
        </View>
      </LottieLoader>



    )
  }
}


