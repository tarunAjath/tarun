import {Dimensions} from 'react-native'

global.primaryColor = '#4A90F4'
global.text_color_light = "#7f7f7f"
global.text_color_normal = "#303030"
global.text_color_dark = "#000"
global.text_color_white='#fff'

global.navigation = null
global.fontFamily_Book = "Roboto-Black"
global.fontFamily_BlackItalic = "Roboto-BlackItalic"
global.fontFamily_Bold = "Roboto-Bold"
global.fontFamily_BoldItalic = "Roboto-BoldItalic"
global.fontFamily_Italic = "Roboto-Italic"
global.fontFamily_Light = "Roboto-Light"
global.fontFamily_LightItalic = "Roboto-LightItalic"
global.fontFamily_Medium = "Roboto-Medium"
global.fontFamily_MediumItalic = "Roboto-MediumItalic"
global.fontFamily_Regular = "Roboto-Regular"
global.fontFamily_Thin = "Roboto-Thin"
global.fontFamily_ThinItalic = "Roboto-ThinItalic"
global.isOnAddPost = false
//global.placeholderText_color="#ADADAD"
global.deviceWidth = Dimensions.get('window').width
global.deviceHeight = Dimensions.get('window').height


