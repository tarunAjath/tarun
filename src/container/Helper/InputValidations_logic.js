let emailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
let fullNameValidation = /^[a-zA-Z _]+$/
let onlyNumbersValidation = /^\d+$/

let errors=[
    {
        fullNameErr:"Name should not contain numbers or special characters",
        phoneErr:"Phone number should be of 10 digit and only contain numbers",
        passErr:"Password should be minimum of 6 chars",
        confirmPassErr:'Password not matching',
        emailErr:"Wrong email format"
    }
]

const ValidationLogics = {
    validationUserName : function(text,callBack){
        if(fullNameValidation.test(text)){
            callBack(true)
        } else {
            callBack(false)
        }
    },

    validationPassword : function(text,callBack){
        if(text.length < 6){
            return false
        } else {
            return true
        }
    },

    validateEmail : function(text,callBack){
        if(emailValidation.test(text)){
            callBack(true)
        } else {
            callBack(false)
        }
    },

    validatePhone : function(text,callBack){
        if(text.length == 10 && onlyNumbersValidation.test(text)){
            callBack(true)
        } else {
            callBack(false)
        }
    },

    validateOnlyNumerics:function(text,callBack){
        if(onlyNumbersValidation.test(text)){
            callBack(true)
        } else {
            callBack(false)
        }
    },

    returnError:function(){
        return errors[0]
    }

}

export default ValidationLogics;