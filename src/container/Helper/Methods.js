import { AsyncStorage, } from 'react-native'
import axios from 'axios'
import UserDataHolder from '../Login_SignUp/UserDataHolder';
import { NavigationActions, StackActions } from 'react-navigation'
import Snackbar from 'react-native-snackbar'

const HelperMethods = {
    makeNetworkCall_post: function (apiName, formData, callBack) {
        axios({
            url: 'https://app-api.nimmio.com/api/user/' + apiName,
            method: 'POST',
            data: formData,
            headers: {
                'Authorization': 'Basic bmltbWVvOkg3S25cUmQrNEpdIi0zWlU=',
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            callBack(response.data)
        }).catch(error => {
            console.log(error)
            // alert('We got error from API')
        })
    },

    makeNetworkCall_get: function (apiName, callBack) {
        axios({
            url: 'http://staging.ajath.com/nimmio/api/user/' + apiName,
            method: 'GET',
        }).then((response) => {
            callBack(response.data)
        })
    },

    navigateHome: function (navigation, accessToken, userId, userName, userEmail, subscribedCat, follower, following, numberOfPosts, about_me,profile_image,date_of_birth,gender) {
     
        UserDataHolder.access_token = accessToken
        UserDataHolder.userId = userId
        UserDataHolder.subscribe_category = subscribedCat
        UserDataHolder.follower = follower
        UserDataHolder.following = following
        UserDataHolder.numberOfPosts = numberOfPosts
        UserDataHolder.name = userName
        UserDataHolder.email = userEmail
        UserDataHolder.about_me = about_me
        UserDataHolder.profile_image = profile_image
        UserDataHolder.date_of_birth = date_of_birth
        UserDataHolder.gender = gender

        
        
        
        AsyncStorage.setItem('userId', userId);
        AsyncStorage.setItem('isLoggedIn', 'true');
        AsyncStorage.setItem('subscribedCat', subscribedCat);
        AsyncStorage.setItem('follower', follower);
        AsyncStorage.setItem('following', following);
        AsyncStorage.setItem('numberOfPosts', numberOfPosts);
        AsyncStorage.setItem('about_me', about_me == '' ? ' ' : about_me);
        AsyncStorage.setItem('userName', userName);
        AsyncStorage.setItem('userEmail', userEmail);
        AsyncStorage.setItem('profile_image', profile_image == '' ? ' ' : profile_image);
        AsyncStorage.setItem('date_of_birth', date_of_birth == '' ? ' ' : date_of_birth);
        AsyncStorage.setItem('gender', gender);

        AsyncStorage.setItem('authToken', accessToken).then(() => {
            navigation.navigate('App')

        })
    },

    navigateToScreen: function (navigation, data, screenName) {
        navigation.navigate(screenName, { cat_name: data })
    },



    updateUserData: function (userName, userEmail, about_me,profile_image) {

        AsyncStorage.setItem('userEmail', userEmail);
        AsyncStorage.setItem('about_me', about_me);
        AsyncStorage.setItem('userName', userName);
        AsyncStorage.setItem('profile_image', profile_image);
        AsyncStorage.setItem('date_of_birth', date_of_birth);
        AsyncStorage.setItem('gender', gender);

    },

    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },

    logout: function (navigation) {
        AsyncStorage.setItem("isLoggedIn", "false").then(() => {
            navigation.navigate('login');
        })
    },


    snackbar: function (message, actionFuncTitle, actionFunc) {
        Snackbar.show({
            backgroundColor: global.primaryColor,
            title: message,
            duration: Snackbar.LENGTH_LONG,
            action: {
                title: actionFuncTitle,
                color: '#fff',
                onPress: () => { actionFunc() }
            },
        });
    },

}


export default HelperMethods;