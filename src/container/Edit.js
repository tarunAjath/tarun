import React, { Component } from "react";
import { View, Text, ScrollView } from 'react-native';
import { Input, Item } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import Entypo from 'react-native-vector-icons/Entypo';
import FAB from 'react-native-fab';

export default class Edit extends Component {
  render() {
    let data = [{
      value: 'British',
    }, {
      value: 'American',
    }, {
      value: 'Indian',
    }];
    let data2 = [{
      value: 'Female',
    }, {
      value: 'Male',
    }
    ];

    return (
      <View style={{ flez: 1, flexDirection: 'column', margin: 15 }}>
        <ScrollView >

          <Text style={{ color: '#000000', marginTop: 120 }}> Name/Username</Text>
          <Item>
            <Input />
          </Item>

          <Text style={{ color: '#000000', marginTop: 12 }}> Bio</Text>
          <Item>
            <Input />
          </Item>
          <Text style={{ color: '#000000', marginTop: 12 }}>
            Interest categories
                </Text>
          <Text style={{ color: '#000000', marginTop: 16 }}>
            Nationality</Text>
          <Dropdown
            data={data} />
          <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'space-between' }}>
            <View style={{marginRight:10, marginTop:10}} >
              <Text style={{ color: '#000000',}}> DOB</Text>
            </View>

            <View style={{marginLeft:10}}>
              <Text style={{ color: '#000000', }}>
                Gender</Text>
              <Dropdown data={data2} />
            </View>
          </View>
          <View style={{ alignItems: 'center', }}>
            <FAB
              small
              buttonColor='blue'
              iconTextColor="#FFFFFF"
              onClickAction={() => this.props.navigation.navigate('post')}
              visible={true}
              iconTextComponent={<Entypo name="plus" />} />
          </View>

        </ScrollView>
      </View>
    );
  }
}