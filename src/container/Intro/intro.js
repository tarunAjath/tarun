import React, { Component } from "react";
import { View, ImageBackground, Button } from "react-native";
import { IndicatorViewPager } from "rn-viewpager";
import Info from "./Pages/info";
import Product from "./Pages/product";
import Service from "./Pages/service";
import Like from "./Pages/like-share-connect";

class Intro extends Component {
  navigateApp = () => {
    this.props.navigation.navigate("App");
  };

  render() {
    return (
      <ImageBackground
        source={require("../../img/Intro_background/intro_background.png")}
        style={{ width: "100%", height: "100%" }}
      >
        {/* button should be in bottom */}
        <View
          style={{
            flex: 1,
            justifyContent: "flex-end",
            alignSelf: "center"
          }}
        >
          <View>
            <Button title="Skip" />
          </View>
        </View>
        <IndicatorViewPager style={{ height: "100%" }}>
          <View>
            <View style={styles.pages}>
              <Info />
            </View>
          </View>

          <View>
            <View style={styles.pages}>
              <Product />
            </View>
          </View>
          <View>
            <View style={styles.pages}>
              <Service />
            </View>
          </View>
          <View>
            <View style={styles.pages}>
              <Like />
            </View>
          </View>
        </IndicatorViewPager>
      </ImageBackground>
    );
  }
}

const styles = {
  viewpager: {
    flex: 1
  },
  pages: {
    margin: 20,

    backgroundColor: "white",
    margin: 40,
    flex: 1,
    borderRadius: 10
  }
};

export default Intro;
