import React, { Component } from "react";
import { Container, Content, Accordion, Button } from "native-base";
import { Text, View, ScrollView } from "react-native";

const dataArray = [
  {
    title: "I don't want to see this Post",
    content:"Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet"
  },
  { title: "This shouldn't be here",
    content: "Lorem ipsum dolor sit amet" },
  { title: "It's Spam",
    content: "Lorem ipsum dolor sit amet" },
  { title: "Other",
    content: "Lorem ipsum dolor sit amet" }
];
export default class ReportPost extends Component {
  render() {
    return (
      <ScrollView>
        <View>
          <View
            style={{
              flex: 1,
              backgroundColor: "#4A90F4",
              marginLeft: 5,
              marginRight: 5,
              height: 10
            }}
          />
          <View style={{ margin: 20 }}>
            <Text style={{ fontSize: 24, fontWeight: "bold", color: "#000" }}>
              Report Post
            </Text>
          </View>
          <Container>
            <Content padder>
              <Accordion
                dataArray={dataArray}
                // expanded={null}
                headerStyle={{ backgroundColor: "#f2f2f2" }}
                contentStyle={{ backgroundColor: "#fff" }}
              />
            </Content>
            <Button>
              <Text>Close</Text>
            </Button>
          </Container>
        </View>
      </ScrollView>
    );
  }
}
