import React from 'react'
import {View} from 'react-native'
import LottieView from 'lottie-react-native';
import '../../container/Helper/Global'
const LottieSpinner = Comp => ({isApiCall,children,...props}) => {
        if(isApiCall){
        return  <Comp {...props}>
                    {children}
                    <View style={styles.overlay}>

                    <LottieView
                        style = {{height :100,width : 70,}}
                        source={require('../../assets/nimmio.json')}
                        autoPlay
                        loop
                        />
                        </View>
                 </Comp>
        } else {
            return (
              <Comp {...props}>
                {children}
              </Comp>
            )
        }
}

const styles = {
    overlay:  {
      flex:1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor : 'rgba(150,150,150, 0.5)',
        justifyContent : 'center',
        alignItems:'center'
       }
}

export default LottieSpinner