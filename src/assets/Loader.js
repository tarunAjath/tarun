import React, { Component } from 'react'
import LottieView from 'lottie-react-native';


export default class Loader extends Component {

    componentDidMount() {
        this.animation.play();
        this.animation.play(30, 120);
      }

    render() {
        <View>
            <LottieView
                ref={animation => {
                    this.animation = animation;
                }}
                source={require('../assets/nimmio.json')}
            />
        </View>

    }
}
